package com.excelsis;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.excelsis.settings.SettingsActivity;

import java.util.HashMap;

import static com.excelsis.RegisterLoaderHelper.*;

@SuppressLint({"SetTextI18n", "DefaultLocale"})
public class DevFTPServerSettings extends AppCompatActivity {
    private static final String DEBUG_TAG = "DevFTPServerSettings";
    private TextView tvFtpServerStatus;
    private EditText edServerHostname;
    private EditText edServerPort;
    private EditText edSerialNumber;
    private CheckBox cbUseFtpServer;
    private Button btnTestFtpConnection;
    private FTPServerSettings sett;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_ftp_server_settings);
        tvFtpServerStatus = findViewById(R.id.tv_server_connection_status);
        edServerHostname = findViewById(R.id.ed_server_hostname);
        edServerPort = findViewById(R.id.ed_server_port);
        edSerialNumber = findViewById(R.id.ed_serial_number);
        cbUseFtpServer = findViewById(R.id.cb_use_ftp_server);
        btnTestFtpConnection = findViewById(R.id.btn_test_ftp_connection);

        // read serial number from app settings so that you don't have to input it again
        edSerialNumber.setText((new AppSettings(this)).getSerialNum());

        sett = new FTPServerSettings(this);
        edServerHostname.setText(sett.getFtpHost());
        edServerPort.setText(Integer.toString(sett.getFtpPort()));
        cbUseFtpServer.setChecked(sett.isUsingFTPServer());
        onUseFtpServerToggled(null);
    }


    public void saveValues(View view) {
        sett.saveAll(
                readHost(),
                readPort(),
                cbUseFtpServer.isChecked());
        Toast.makeText(this, "Settings saved", Toast.LENGTH_SHORT).show();
    }

    private int readPort() {
        String portStr = edServerPort.getText().toString();
        int port = 21;
        try {
            port = Integer.parseInt(portStr);
        } catch(NumberFormatException ex) {
            String errMsg = String.format(
                    "Unable to parse port: '%s'\n" +
                            "Using default value %d",
                    portStr, port);
            Toast.makeText(this, errMsg, Toast.LENGTH_SHORT).show();
            edServerPort.setText(Integer.toString(port));
        }
        return port;
    }

    private String readHost() {
        return edServerHostname.getText().toString();
    }

    public void testGettingSettingsFile(View view) {
        saveValues(null);
        tvFtpServerStatus.setText("Trying to get the file ...");
        String deviceId = edSerialNumber.getText().toString();

        FallbackLoader loader = new FallbackLoader(this, deviceId);
        loader.load((success, eventLog) -> {
            runOnUiThread(() -> {
                String msg = String.format(
                        "Success: %s\n Log:\n%s\n",
                        success,
                        TextUtils.join("\n-", eventLog)
                );

                HashMap<String, Register> regs = RegisterLibrary.getSettingsRegisters().getRegisters();
                msg += "Loaded " + (regs==null?0:regs.size()) + " registers\n";
                tvFtpServerStatus.setText(msg);
            });
        });
    }


    public void testFtpServerConnection(View view) {
        RegisterLoaderFTP registerLoaderFTP =
                new RegisterLoaderFTP(readHost(), readPort());

        tvFtpServerStatus.setText("Connecting ...");
        Thread thread = new Thread(() -> {
            String serverStatus;
            try {
                // try to connect to the server and do a simple operation
                registerLoaderFTP.connect();
                String fileList = registerLoaderFTP.listFiles();
                serverStatus = "Connection looks good\n" +
                                "Got files:\n" + fileList;
                registerLoaderFTP.disconnect();
            } catch(Exception ex) {
                serverStatus = "Error:\n" + ex.toString();
                ex.printStackTrace();
                Log.e(DEBUG_TAG, ex.toString());
            }

            String finalServerStatus = serverStatus;
            runOnUiThread(() -> {
                tvFtpServerStatus.setText(finalServerStatus);
            });
        });
        thread.start();
    }

    public void onUseFtpServerToggled(View view) {
        edServerHostname.setEnabled(cbUseFtpServer.isChecked());
        edServerPort.setEnabled(cbUseFtpServer.isChecked());
        btnTestFtpConnection.setEnabled(cbUseFtpServer.isChecked());
    }

    public static class FTPServerSettings {
        private static final String FTP_SETTINGS = "FTP_SETTINGS";
        Context context;

        public static final String FTP_HOST  = "FTP_HOST";
        public static final String FTP_HOST_DEF  = "192.168.1.50";
        public static final String FTP_PORT = "FTP_PORT";
        public static final String USE_FTP_SERVER = "USE_FTP_SERVER";
        public static final int FTP_PORT_DEF = 21;

        public FTPServerSettings(Context context) {
            if(context == null) throw new NullPointerException();
            this.context = context;
        }

        private SharedPreferences getFtpSettingsSP() {
            return  context.getSharedPreferences(FTP_SETTINGS, Context.MODE_PRIVATE);
        }

        public String getFtpHost() {
            return getFtpSettingsSP().getString(FTP_HOST, FTP_HOST_DEF);
        }

        public int getFtpPort() {
            return getFtpSettingsSP().getInt(FTP_PORT, FTP_PORT_DEF);
        }

        public boolean isUsingFTPServer() {
            return getFtpSettingsSP().getBoolean(USE_FTP_SERVER, false);
        }

        public void saveAll(String host, int port, boolean isUsingFtpServer) {
            SharedPreferences sp = getFtpSettingsSP();
            SharedPreferences.Editor editor = sp.edit();

            editor.putString(FTP_HOST, host);
            editor.putInt(FTP_PORT, port);
            editor.putBoolean(USE_FTP_SERVER, isUsingFtpServer);
            editor.apply();
        }
    }
}