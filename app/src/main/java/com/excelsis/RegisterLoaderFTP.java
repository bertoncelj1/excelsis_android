package com.excelsis;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.InetAddress;

public class RegisterLoaderFTP {

    public String host;
    public int port;

    public static final String USER_ID = "anonymous";
    private static final String USER_PASSWORD = "";

    FTPClient ftpClient;

    public RegisterLoaderFTP(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public static RegisterLoaderFTP createFromSavedSettings(Context context) {
        DevFTPServerSettings.FTPServerSettings ftpServerSettings =
                new DevFTPServerSettings.FTPServerSettings(context);

        return new RegisterLoaderFTP(
                ftpServerSettings.getFtpHost(),
                ftpServerSettings.getFtpPort()
        );
    }

    public void connect() throws IOException {
        ftpClient = new FTPClient();
        ftpClient.connect(host, port);
        if(!ftpClient.login(USER_ID, USER_PASSWORD)) {
            ftpClient.logout();
            throw new IOException("Unable to connect to the server: "+ getServerSpecs());
        }
    }

    public String getServerSpecs() {
        return String.format("(%s:%d): user:%s, pass:%s",
                host, port, USER_ID, USER_PASSWORD);
    }

    public void disconnect() throws IOException {
        if(ftpClient != null) {
            ftpClient.disconnect();
        }
    }

    public String listFiles() {
        if (ftpClient == null) return "no ftp client";

        String out = "Files:\n";
        FTPFile[] files = new FTPFile[0];
        try {
            files = ftpClient.listFiles("/");
            for (FTPFile file : files) {
                out += "Ftp file" + file.getName() + "\n";
            }

        }catch (IOException e) {
            e.printStackTrace();
        }

        return out;
    }


    public Reader getFileReader(String fileName) throws IOException {
        if (ftpClient == null) return null;

        InputStream is = ftpClient.retrieveFileStream(fileName);

        if (is == null) {
            throw new IOException(
                    String.format("Failed loading file: %s. Server:%s",
                            fileName, getServerSpecs())
            );
        }
        return new InputStreamReader(is);

    }



}
