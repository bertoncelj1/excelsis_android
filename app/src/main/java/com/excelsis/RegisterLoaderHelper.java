package com.excelsis;

import android.content.Context;
import android.util.Log;

import java.io.Reader;
import java.util.ArrayList;

import static com.excelsis.RegisterLibrary.getDecodeRegisters;
import static com.excelsis.RegisterLibrary.getReadRegisters;
import static com.excelsis.RegisterLibrary.getSettingsRegisters;

public class RegisterLoaderHelper {

    public static class File {

        private static final String DEBUG_TAG = "RegisterLoaderHelper";

        private static RegisterLoaderFile.RegistersType parserToRegistersType(RegisterLibrary.RegisterParser parser) {
            switch (parser.getType()) {
                case SETTINGS:
                    return RegisterLoaderFile.RegistersType.SETTINGS;
                case DECODING:
                    return RegisterLoaderFile.RegistersType.DECODING;
                case READ:
                    return RegisterLoaderFile.RegistersType.READ;
            }
            return null;
        }

        static class ReadingRegistersException extends RuntimeException {
            public ReadingRegistersException(String msg) {
                super(msg);
            }
        }

        public static void readRegisterFromFile(Context context, RegisterLibrary rl) {
            readRegisterFromFile(context, rl, null);
        }

        public static void readRegisterFromFile(Context context, RegisterLibrary rl, String filePath) {
            if(filePath == null) {
                filePath = RegisterLoaderFile.getRegisterFilePath(parserToRegistersType(rl.registerParser));
            }

            Log.v(DEBUG_TAG, "Loading registers: " + filePath);

            boolean success = rl.readFromFile(RegisterLoaderFile.APP_FOLDER_NAME + "/" + filePath);
            if (!success) {
                Log.w(DEBUG_TAG, "Unable to read registers from external storage. Reading them from assets");
                rl.readFromAssets(context, filePath);
            }
        }

        public static void loadSettingsRegisters(Context context, String filePath) {
            readRegisterFromFile(context, getSettingsRegisters(), filePath);
        }

        public static void loadSettingsRegisters(Context context) {
            readRegisterFromFile(context, getSettingsRegisters());
        }

        public static void loadReadRegisters(Context context) {
            readRegisterFromFile(context, getReadRegisters());
        }

        public static void loadDecodeRegisters(Context context) {
            readRegisterFromFile(context, getDecodeRegisters());
        }

    }

    public static class FTP {

        public static void readRegisterFromFTP(
                RegisterLoaderFTP ftp,
                RegisterLibrary rl,
                String filename,
                TaskListener listener
        ) {
            Thread thread = new Thread(() -> {
                try {
                    ftp.connect();
                    Reader reader = ftp.getFileReader(filename);
                    if (reader != null) {
                        rl.read(reader);
                        listener.threadComplete(true, "");
                    } else {
                        String errmsg = "Unable to read registers from FTP. Library:" +
                                rl.registerParser.getType() +
                                " filename:" + filename;
                        Log.w("ftp", errmsg);
                        listener.threadComplete(false, errmsg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                    listener.threadComplete(false, e.getMessage());
                }
            });
            thread.start();
        }

        public static void loadSettingsRegisters(Context context, String filename, TaskListener listener) {
            readRegisterFromFTP(
                    RegisterLoaderFTP.createFromSavedSettings(context),
                    getSettingsRegisters(),
                    filename,
                    listener
            );
        }

        public interface TaskListener {
            void threadComplete(boolean success, String message);
        }

    }


    public static class FallbackLoader {
        private static final String DEBUG_TAG = "RegisterFallbackLoader";

        Context context;
        private final String fileName;
        private final String fileNameFallback;
        int fallbackNumber;
        LoadingRegisters callback;
        ArrayList<String> eventsLog;

        public FallbackLoader(Context context, String deviceId) {
            this.context = context;
            fileName = deviceId + ".csv";
            fileNameFallback = deviceId.substring(0, 2) + ".csv";
        }

        public interface LoadingRegisters {
            void registersLoaded(boolean success, ArrayList<String> errors);
        }



        public void load(LoadingRegisters callback) {
            this.callback = callback;
            fallbackNumber = 0;
            eventsLog = new ArrayList<>();
            load();
        }

        void nextTry() {
            fallbackNumber++;
            load();
        }

        private void load() {
            eventsLog.add("\n");
            eventsLog.add("Try: " + fallbackNumber);
            switch (fallbackNumber) {
                case 0:
                    this.loadFTP(fileName);
                    break;

                case 1:
                    this.loadFTP(fileNameFallback);
                    break;

                case 2:
                    loadFromFiles(fileName);
                    break;

                case 3:
                    loadFromFiles(fileNameFallback);
                    break;

                case 4:
                    eventsLog.add("No more other options. Loading failed");
                    callback.registersLoaded(false, eventsLog);
            }
        }

        private void loadFromFiles(String fileName) {
            eventsLog.add("From Files " + fileName);
            try {
                RegisterLoaderHelper.File.loadSettingsRegisters(context, fileName);
                callback.registersLoaded(true, eventsLog);
            } catch (RegisterLoaderHelper.File.ReadingRegistersException e) {
                String msg = "Failed to load settings file:" + e.getMessage();
                Log.w(DEBUG_TAG, msg);
                eventsLog.add("Error: " + msg);
                nextTry();
            }
        }

        private void loadFTP(String fileName) {
            eventsLog.add("From FTP " + fileName);
            if (isUsingFTPServer()) {
                RegisterLoaderHelper.FTP.loadSettingsRegisters(
                        context,
                        fileName,
                        FTPcallback);
            } else {
                eventsLog.add("FTP is not being used");
                nextTry();
            }
        }

        RegisterLoaderHelper.FTP.TaskListener FTPcallback = (success, message) -> {
            if (success) {
                callback.registersLoaded(true, eventsLog);
            } else {
                String msg = "Failed to load FTP setting:\n" + message;
                Log.w(DEBUG_TAG, msg);
                eventsLog.add("Error: " + msg);
                nextTry();
            }
        };

        private boolean isUsingFTPServer() {
            DevFTPServerSettings.FTPServerSettings ftpServerSettings =
                    new DevFTPServerSettings.FTPServerSettings(context);
            return ftpServerSettings.isUsingFTPServer();
        }

    }
}
