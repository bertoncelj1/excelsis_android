package com.excelsis;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.os.Build.VERSION.SDK_INT;
import static com.excelsis.RegisterLibrary.getReadRegisters;
import static com.excelsis.RegisterLibrary.getSettingsRegisters;

public class RegisterLoaderFile extends RegisterLoader {

    public static final String APP_FOLDER_NAME = "excelsis";
    public static final String SETTINGS_REGISTERS_FILE_LOCATION = "settings_registers.csv";
    public static final String DECODING_REGISTERS_FILE_LOCATION = "decoding_registers.csv";
    public static final String READ_REGISTERS_FILE_LOCATION = "read_registers.csv";
    private static final String DEBUG_TAG = "RegisterLoader";

    public static enum RegistersType {
        SETTINGS, DECODING, READ
    }

    public static String getRegisterFilePath(RegistersType location) {
        switch (location) {
            case SETTINGS: return SETTINGS_REGISTERS_FILE_LOCATION;
            case DECODING: return DECODING_REGISTERS_FILE_LOCATION;
            case READ: return READ_REGISTERS_FILE_LOCATION;
        }
        return null;
    }


    private static File createFolder(File folder) throws IOException {
        Log.v(DEBUG_TAG, "Creating new folder: " + folder.getAbsolutePath());
        if (!folder.exists()) {
            boolean success = folder.mkdir();
            //SecurityManager security = System.getSecurityManager();
            //if (security != null) {
            //    security.checkWrite(folder.getPath());
            //}

            if (success) {
                Log.d(DEBUG_TAG, "Created new folder on external dir: " + folder.getAbsolutePath());
            } else {
                Log.e(DEBUG_TAG, "Failed to create folder:" + folder.getAbsolutePath());
                throw new IOException("failed to create folder " + folder.getAbsolutePath());
            }
        } else {
            Log.v(DEBUG_TAG, "Folder already exists, We are not creating it again: " + folder.getAbsolutePath());
        }
        return folder;
    }


    public static boolean requestPermisionsSDK_30(Activity activity) {
        if(SDK_INT >= 30){
            if(!Environment.isExternalStorageManager()){
                Snackbar.make(activity.findViewById(android.R.id.content), "Permission needed!", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                try {
                                    Uri uri = Uri.parse("package:" + BuildConfig.APPLICATION_ID);
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, uri);
                                    activity.startActivity(intent);
                                } catch (Exception ex){
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                                    activity.startActivity(intent);
                                }
                            }
                        })
                        .show();
                return false;
            }
        }

        return true;
    }

    public static boolean requestStoragePermissions(Activity activity) {
        if(!requestPermisionsSDK_30(activity)) return false;


        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if(ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[] { permission }, 123);
            Toast.makeText(activity, "No storage permissions!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static File getAppFolder() {
        return new File(Environment.getExternalStorageDirectory(), APP_FOLDER_NAME);
    }


    public static void copyAssetsToExternalSD(Activity activity) {
        boolean hasStoragePermissions = requestStoragePermissions(activity);
        if(!hasStoragePermissions) {
            return;
        }

        File outFolder = getAppFolder();
        try {
            copyAssets(activity.getAssets(), createFolder(outFolder));
            Toast.makeText(activity, "Assets copied to: " + getAppFolder().getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(
                    activity,
                    "Error copying assets to external storage. Error:" + e.getMessage(),
                    Toast.LENGTH_LONG
            ).show();
        }
    }

    public static boolean copyAssets(AssetManager assetManager,
                                     File targetFolder) throws IOException {
        Log.i(DEBUG_TAG, "Copying files from assets to folder " + targetFolder);
        return copyAssets(assetManager, "", targetFolder);
    }

    /**
     * The files will be copied at the location targetFolder+path so if you
     * enter path="abc" and targetfolder="sdcard" the files will be located in
     * "sdcard/abc"
     */
    public static boolean copyAssets(AssetManager assetManager,
                                     String path,
                                     File targetFolder) throws IOException {
        if (targetFolder == null) return false;

        Log.i(DEBUG_TAG, "Copying " + path + " to " + targetFolder);
        String[] sources = assetManager.list(path);
        if (sources.length == 0) { // its not a folder, so its a file:
            copyAssetFileToFolder(assetManager, path, targetFolder);
        } else { // its a folder:
            if (path.startsWith("images") || path.startsWith("sounds")
                    || path.startsWith("webkit")) {
                Log.i(DEBUG_TAG, "  > Skipping " + path);
                return false;
            }
            File targetDir = new File(targetFolder, path);
            targetDir.mkdirs();
            for (String source : sources) {
                String fullSourcePath = path.equals("") ? source : (path
                        + File.separator + source);
                copyAssets(assetManager, fullSourcePath, targetFolder);
            }
        }
        return true;
    }

    private static void copyAssetFileToFolder(AssetManager assetManager,
                                              String fullAssetPath,
                                              File targetBasePath) throws IOException {
        InputStream in = assetManager.open(fullAssetPath);
        OutputStream out = new FileOutputStream(new File(targetBasePath,
                fullAssetPath));
        byte[] buffer = new byte[16 * 1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        out.flush();
        out.close();
    }

}
