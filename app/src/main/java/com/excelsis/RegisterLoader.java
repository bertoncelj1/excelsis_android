package com.excelsis;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class RegisterLoader {

    public static enum RegistersType {
        SETTINGS, DECODING, READ
    }

}
