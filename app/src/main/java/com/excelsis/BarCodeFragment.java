package com.excelsis;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Hashtable;
import java.util.Map;

import static com.excelsis.Utils.whatsWrongWithSerialNumber;

public class BarCodeFragment extends Fragment {

    final static String DEBUG_TAG = "QrCodeFragment";
    // camera permission needed for QR code scanner
    final static int PERMISSION_REQUEST_CODE = 1;
    final static String[] CAMERA_PERMISSION = new String[]{"android.permission.CAMERA"};
    private boolean mPendingQrScan; // if are waiting for user to give CAMERA permission

    public TextView mSerialNumber;
    public TextView mTelephoneNumber;


    // can user manually input fields that are meant to be input from QrCode
    private boolean manualInput = true;

    // links QrCodeData fields to TextView fields
    private final Map<Integer, TextView> mQrCodeFields = new Hashtable<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mRootView = inflater.inflate(R.layout.qrcode_fragment, container, false);

        mSerialNumber = mRootView.findViewById(R.id.setting_serial_number);
        mTelephoneNumber = mRootView.findViewById(R.id.setting_telephone_number);

        mSerialNumber.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void afterTextChanged(Editable s) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String whatsWrong = Utils.whatsWrongWithSerialNumber(s.toString());
                if(whatsWrong != null) {
                    mSerialNumber.setError(whatsWrong);
                }
            }
        });

        ImageView mScanQrCode = mRootView.findViewById(R.id.scan_qr_code);
        mScanQrCode.setOnClickListener(v -> {
            // clear all text fields that are going to be filled with qrCode data
            mSerialNumber.setText("");
            startQrScanner();
        });

        loadTextEditValues();
        return mRootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveTextEditValues();
    }

    public void saveTextEditValues() {
        AppSettings appSettings = new AppSettings(getContext());
        String telNum = mTelephoneNumber.getText().toString();
        String serNum = mSerialNumber.getText().toString();
        appSettings.saveTelAndSerial(telNum, serNum);
    }

    public void loadTextEditValues() {
        AppSettings appSettings = new AppSettings(getContext());

        if(appSettings.isRememberingValuesSet()) {
            // only load values if remembering values is set
            mTelephoneNumber.setText(appSettings.getTelephoneNum());
            mSerialNumber.setText(appSettings.getSerialNum());
        }
    }

    /*
    Start QrScanner application
     */
    private void startQrScanner(){
        mPendingQrScan = true;

        if(hasCameraPermission()){
            mPendingQrScan = false;

            IntentIntegrator integrator = new IntentIntegrator(this.getActivity());
            integrator.setOrientationLocked(false);
            integrator.setPrompt("Scan device's serial number");
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);

            integrator.initiateScan();
        } else {
            requestPermissions();
        }
    }


    /**
     * Enables user to put in serial number, thephone number ... fields manualy
     * instead of from QR code
     * @param enable or disable manual input
     */
    private void enableManualInput(boolean enable){
        manualInput = enable;
        mSerialNumber.setEnabled(enable);
        mTelephoneNumber.setEnabled(enable);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private boolean hasCameraPermission(){
        return ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1){
            requestPermissions(CAMERA_PERMISSION, PERMISSION_REQUEST_CODE);
        } else {
            Log.e(DEBUG_TAG, "Requesting permissions even though SDK < LOLLIPOP_MR1");
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults){
        switch(permsRequestCode){
            case PERMISSION_REQUEST_CODE:
                boolean permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if(mPendingQrScan) {
                    if(permissionGranted) {
                        startQrScanner();
                    }else{
                        Toast.makeText(this.getContext(), "Don't have permissions to use camera",
                                Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        readBarCode(scanningResult);
    }

    /*
    Reads QrCode and writes read data into TextViews
     */
    private void readBarCode(IntentResult scanningResult){
        if(scanningResult == null || scanningResult.getContents() == null){
            Log.e(DEBUG_TAG, "Failed to scan. scanning result is null");
            Toast.makeText(getContext(), "Failed to scan", Toast.LENGTH_SHORT).show();
        } else {

            String serialNumber = scanningResult.getContents();
            Log.v(DEBUG_TAG, "Scanned:" + serialNumber);


            if(serialNumber.length() > 20) {
                Log.e(DEBUG_TAG, "The scanning contents are not serial number. Contents:" + serialNumber);
                Toast.makeText(getContext(), "Length > 20. Definitely not a serial number", Toast.LENGTH_LONG).show();
                return;
            }

            mSerialNumber.setText(serialNumber);
        }
    }


}
