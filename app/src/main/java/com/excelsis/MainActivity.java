package com.excelsis;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.excelsis.RegisterLoaderHelper.FallbackLoader;
import com.excelsis.settings.SettingsActivity;
import com.excelsis.sms.creator.MessageComponents;
import com.excelsis.sms.creator.SmsMessageCreator;
import com.excelsis.sms.SmsSenderActivity;
import com.excelsis.sms.creator.bodyGenerators.TypeReadBody;
import com.excelsis.sms.creator.bodyGenerators.TypeMacroGenerator;
import com.excelsis.Utils;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {


    private static final String DEBUG_TAG = "MainActivity";
    private BarCodeFragment barCodeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barCodeFragment = (BarCodeFragment) getSupportFragmentManager().findFragmentById(R.id.barCodeFragment);

        // TODO nevem če je to najboljše mesto da se to kliče ... mislm da skoraj zagotovo ne
        // jaz bi to dala da se naloži takrat ko se potrebuje, kar pomeni v decoding activity
        RegisterLoaderHelper.File.loadDecodeRegisters(this);
    }

    public void initSettingsRegistersPreferences() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sp.edit();

        // clear all the preferences that are currently set
        editor.clear().apply();

        // read settings registers from library and set their default values to the preferences
        RegisterLibrary rl = RegisterLibrary.getSettingsRegisters();

        for (Register register : rl.getRegistersArray()) {
            if (register.preferenceKey.equals("ALARM_MASK_E2")
                    || register.preferenceKey.equals("ALARM_MASK_E4")
                    || register.preferenceKey.equals("ALARM_MASK_E8")) {
                // alarm mask is a special case and we can't just pass string to the preferences
                // we need to convert it first to an set string.
                editor.putStringSet(register.preferenceKey, Utils.encodeAlarmMask(register.defaultValue));
            } else {
                editor.putString(register.preferenceKey, register.defaultValue); // set default value
            }
        }
        editor.apply();
    }



    public void sendSettings(View v) {
        try {
            if(isSomethingWrongWithTelOrSerial()) return;

            Button openSettingsButton = findViewById(R.id.openSettings);

            CharSequence buttonTxt = openSettingsButton.getText();
            openSettingsButton.setEnabled(false);
            openSettingsButton.setText(buttonTxt.toString() + " loading ...");

            FallbackLoader loader = new FallbackLoader(this, getSerialNum());
            loader.load((success, eventLog) -> {
                runOnUiThread(() -> {
                            if (success) {
                                Toast.makeText(this, "Loaded OK", Toast.LENGTH_SHORT).show();
                                initSettingsRegistersPreferences();
                                startActivity(new Intent(this, SettingsActivity.class));
                            } else {
                                Toast.makeText(
                                        this,
                                        "ERROR: Failed to open settings! Check serial number.",
                                        Toast.LENGTH_LONG).show();
                            }

                            openSettingsButton.setEnabled(true);
                            openSettingsButton.setText(buttonTxt.toString());
                        }
                );
            });
        } catch (Exception e) {
            Toast.makeText(
                    this,
                    "ERROR: Failed to open settings! " + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            Log.e(DEBUG_TAG, "Unable to open settings! " + e.getMessage());
            e.printStackTrace();
        }

    }

    public String getTelephoneNum() {
        return barCodeFragment.mTelephoneNumber.getText().toString();
    }

    public String getSerialNum() {
        return barCodeFragment.mSerialNumber.getText().toString();
    }

    public String checkTelAndSerNumbers() {
        String telephoneNumber = getTelephoneNum();
        String whatsWrong = Utils.whatsWrongWithPhoneNumber(telephoneNumber);
        if (whatsWrong != null) {
            String msg = "Invalid telephone number: " + telephoneNumber;
            Log.e(DEBUG_TAG, msg);
            return msg;
        }

        String serialNumber = getSerialNum();
        whatsWrong = Utils.whatsWrongWithSerialNumber(serialNumber);
        if (whatsWrong != null) {
            String msg = "Invalid serial number! Serial:" + serialNumber;
            Log.e(DEBUG_TAG, msg);
            return msg;
        }

        return null;
    }

    public boolean isSomethingWrongWithTelOrSerial() {
        String whatsWrong = checkTelAndSerNumbers();
        if(whatsWrong != null) {
            Toast.makeText(this, "Error: " + whatsWrong, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public void sendMessage(SmsMessageCreator smsMessageCreator) {
        // make sure we first save(refresh) telephone nubmer into shared preferences by calling
        // saveTextEditValues before reading from them
        //barCodeFragment.saveTextEditValues();

        if(isSomethingWrongWithTelOrSerial()) return;

        smsMessageCreator.serialNumber = getSerialNum();

        try {
            MessageComponents.Messages messages = smsMessageCreator.getIndependentMessages();
            byte[][] byteMessages = messages.getBytes();
            String messageComponents = messages.getPrettyPrintMessage();
            Log.d(DEBUG_TAG, messageComponents);

            for (int i = 0; i < byteMessages.length; i++) {
                Log.d(DEBUG_TAG, "Message" + i + " :" + Utils.arrayToString(byteMessages[i]));
            }

            SmsSenderActivity.sendMessages(this, byteMessages, getTelephoneNum(), messageComponents);
        } catch (Exception e) {
            Log.e(DEBUG_TAG, "Error composing message " + e.getMessage(), e);
            Toast.makeText(this, "Error composing message " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void sendMacroModbusTrigger(View v) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        // configure SmsMessageCreator
        SmsMessageCreator smsMessageCreator = new SmsMessageCreator(this);
        byte password = (byte) 0x84;
        TypeMacroGenerator body = new TypeMacroGenerator(
                TypeMacroGenerator.Command.TRIGGER_MB, password);
        smsMessageCreator.setMessageBodyGenerator(body);

        sendMessage(smsMessageCreator);
    }

    public void sendMacroResetTrigger(View v) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        // configure SmsMessageCreator
        SmsMessageCreator smsMessageCreator = new SmsMessageCreator(this);
        byte password = (byte) 0x84;
        TypeMacroGenerator body = new TypeMacroGenerator(
                TypeMacroGenerator.Command.RESET_SMS, password);
        smsMessageCreator.setMessageBodyGenerator(body);

        sendMessage(smsMessageCreator);
    }

    public void sendReadRegisters(View v) {
        try {
            if(isSomethingWrongWithTelOrSerial()) return;

            RegisterLoaderHelper.File.loadReadRegisters(this);
            RegisterLibrary rl = RegisterLibrary.getReadRegisters();

            // configure SmsMessageCreator
            SmsMessageCreator smsMessageCreator = new SmsMessageCreator(this);
            TypeReadBody body = new TypeReadBody();
            body.setRegistersToSend(rl.getRegistersArrayOrdered());
            smsMessageCreator.setMessageBodyGenerator(body);

            sendMessage(smsMessageCreator);
        } catch (Exception e) {
            Toast.makeText(
                    this,
                    "ERROR: Failed to read registers! " + e.getMessage(),
                    Toast.LENGTH_LONG).show();
            Log.e(DEBUG_TAG, "ERROR: Unable to read registers! " + e.getMessage());
            e.printStackTrace();
        }
    }


    public void openDevSettings(View v) {
        startActivity(new Intent(this, DevSettingsActivity.class));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        // pass all the activity result calls (just the scan bar code) to the fragment
        barCodeFragment.onActivityResult(requestCode, resultCode, intent);
    }
}