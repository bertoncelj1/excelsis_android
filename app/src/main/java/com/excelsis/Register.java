package com.excelsis;

import java.util.Locale;

public class Register {

    // these two parameters are final since they define register
    public final String preferenceKey;

    public int channel = -1;
    public int register = -1;
    public int dataType = -1; // register data type, basically how to encode/devode register
    public String defaultValue; // value read from the settings
    public String value; // value that is going to be send
    public String description = "";

    public Register(String preferenceKey){
        this.preferenceKey = preferenceKey;
    }

    /**
     * Extract register from key
     * @param key
     * @return channel value
     */
    public static int getChanel(int key){
        return key / 100000;
    }

    /**
     * Extract register from key
     * @param key
     * @return register value
     */
    public static int getRegister(int key){
        return key % 100000;
    }


    /**
     * Some of the registers are not actually registers but are just values used to
     * for example show and hide sections
     *
     * If the register is not an actual register id doesn't have channel and key value set
     */
    public boolean isAnActualRegister() {
        return channel != -1 && register != -1;
    }

    public int getKey(){
        return getKey(channel, register);
    }

    public static int getKey(int channel, int register){
        // check if arguments are within range
        if((register & 0xFFFF) != register) throw new IllegalArgumentException("Register is larger than two bytes");
        if((channel & 0xFF) != channel) throw new IllegalArgumentException("Channel is larger than one byte!");

        return channel * 100000 + register;
    }

    /*
    Get key form channel and register in string format.
    They are in string format for easier input
     */
    public static int getKey(String channel, String register){
        int channelInt = Integer.parseInt(channel, 16);
        int registerInt = Integer.parseInt(register, 16);

        return getKey(channelInt, registerInt);
    }

    /*
    Get key in readable form
     */
    public static String keyToString(int key){
        int channel = getChanel(key);
        int register = getRegister(key);

        return String.format(Locale.getDefault(),
                "ch:%02X, reg:%04X",
                channel,
                register );
    }

    public String keyToString(){
        return String.format(Locale.getDefault(),
                "ch:%02X, reg:%04X",
                channel,
                register );
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(),
                "ch:'%s', reg:'%s', %s",
                channel == -1 ? "/" : String.format("%02X", channel) ,
                register == -1 ? "/" : String.format("%04X", register),
                preferenceKey);
    }

    public String toStringDescription() {
        return String.format(Locale.getDefault(),
                "ch:'%s', reg:'%s', %s",
                channel == -1 ? "/" : String.format("%02X", channel) ,
                register == -1 ? "/" : String.format("%04X", register),
                description);
    }
}
