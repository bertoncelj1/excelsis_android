package com.excelsis.displayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.excelsis.MainActivity;
import com.excelsis.R;
import com.excelsis.RegisterLibrary;
import com.excelsis.RegisterLoaderHelper;
import com.excelsis.Utils;
import com.excelsis.sms.decoder.BodyData;
import com.excelsis.sms.decoder.BodyDataAck;
import com.excelsis.sms.decoder.BodyDataRegisters;
import com.excelsis.sms.decoder.SmsMessageDecoder;

/**
 * Created by anze on 10/9/16.
 */
public class ActivitySmsDisplayer extends Activity {

    private static final String DEBUG_TAG = "SmsDisplayer";
    
    TextView mFromTextView;
    TextView mInfo;

    public static void showFakeMessage(Context context, byte[] binaryMessage) {
        Intent showMessage = new Intent(context, ActivitySmsDisplayer.class);
        showMessage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        showMessage.putExtra("from", "myself :)");
        showMessage.putExtra("binaryMessage", binaryMessage);
        context.startActivity(showMessage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);


        String from = null;
        String messageStr = null;
        byte[] binaryMessage = null;

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            from = bundle.getString("from");
            messageStr = bundle.getString("message");
            binaryMessage = bundle.getByteArray("binaryMessage");

            // write binary message in message if it is null
            if(messageStr == null && binaryMessage != null){
                messageStr = Utils.arrayToString(binaryMessage);
            }
        }else{
            Log.e(DEBUG_TAG, "Unable to get bundle");
            //finish();
            return;
        }

        Log.d(DEBUG_TAG, "Recived message from " + from);
        Log.d(DEBUG_TAG, "Recived message " + messageStr);

        mFromTextView = findViewById(R.id.from);
        mInfo = findViewById(R.id.infoMessage);

        mFromTextView.setText(from == null? "unknown" : from);

        String infoMessage = "";
        mInfo.setText(binaryMessage == null? "no binary message received" : "");

        // encode message and show decoded data
        if(binaryMessage == null) {
            infoMessage = "no binary message received";
        } else {
            SmsMessageDecoder decoder = new SmsMessageDecoder(this);

            try {
                SmsMessageDecoder.Message message = decoder.decodeMessage(binaryMessage);

                infoMessage = displayBody(message);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                Log.e(DEBUG_TAG, "Error occurred while decoding: " + e.getMessage(), e);
                infoMessage = "Error occurred while decoding: " + e.getMessage();
            }
        }

        if(infoMessage.equals("")) {
            mInfo.setVisibility(View.GONE);
        } else {
            mInfo.setText(infoMessage);
        }
    }

    public String displayBody(SmsMessageDecoder.Message message) {
        try {
            String infoMessage = "type: " + message.messageType.toString().toLowerCase() + "\n\n";
            switch(message.messageType) {
                case ACKNOWLEDGE:
                    infoMessage += displayAckBody((BodyDataAck) message.bodyData);
                    break;

                case REGISTER_READ_RESPONSE:
                case REGISTER_WRITE_ANDROID:
                case REGISTER_WRITE_ANDROID_END:
                case REGISTER_WRITE:
                    // we need to load read registers so that we can parse registers inside body
                    RegisterLoaderHelper.File.loadDecodeRegisters(this);
                    infoMessage += displayRegisterReadResponse((BodyDataRegisters) message.bodyData);
                    break;


                default:
                    infoMessage += displayUnknownBody(message.bodyData);
            }

            return infoMessage;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(DEBUG_TAG, "Unable to display message body: " + e.getMessage());
            return "Unable to display message body: " + e.getMessage();
        }
    }

    private String displayRegisterReadResponse(BodyDataRegisters bodyDataRegisters) {
        RecyclerView recyclerView = findViewById(R.id.display_registers_list);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this,  bodyDataRegisters.registers);
        recyclerView.setAdapter(adapter);

        if(bodyDataRegisters.parsingMessageErr != null)
            return bodyDataRegisters.parsingMessageErr + "\nRaw data: " + bodyDataRegisters;
        return "";
    }

    @SuppressLint("DefaultLocale")
    public String displayAckBody(BodyDataAck bodyDataAck) {
        return String.format("\tis acknowledged: %s (0x%02X)\n", bodyDataAck.ackOK, bodyDataAck.ackOKint);
    }

    public String displayUnknownBody(BodyData bodyData) {
        return "\tDon't know how to display this message body type. Raw data:" + bodyData;
    }


    public void onReturnToHomeClicked(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }
}
