package com.excelsis.displayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.excelsis.R;
import com.excelsis.Register;
import com.excelsis.sms.decoder.SmsMessageDecoder;

import java.util.ArrayList;


/**
 * Created by sonu on 19/09/16.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView registerNumber;
        private TextView registerName;
        private TextView registerValue;

        RecyclerViewHolder(View view) {
            super(view);
            registerNumber = view.findViewById(R.id.row_item_message_register_number);
            registerName = view.findViewById(R.id.row_item_message_register_name);
            registerValue = view.findViewById(R.id.row_item_message_value);
        }

    }

    private ArrayList<SmsMessageDecoder.RegisterData> registerList;

    Context context;

    public RecyclerViewAdapter(Context context, ArrayList<SmsMessageDecoder.RegisterData> bodyDataRegisters) {
        this.context = context;
        this.registerList = bodyDataRegisters;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_message_register, viewGroup, false);
        return new RecyclerViewHolder(v);
    }

    public String userPrettyPrintName(String registerName) {
        registerName = registerName.replaceAll("_", " ");
        registerName = registerName.replaceAll("([a-z])([A-Z0-9]+)", "$1 $2"); // separate camel case with spaces
        registerName = registerName.replaceAll("([0-9])([A-z])", "$1 $2"); // separate numbers with spaces
        registerName = registerName.toLowerCase();
        return registerName.substring(0, 1).toUpperCase() + registerName.substring(1); // set first char to uppercase
    }


    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int i) {
        final SmsMessageDecoder.RegisterData rd = registerList.get(i);
        Register register = rd.register;

        holder.registerNumber.setText(String.format("(%02X:%04X)", register.channel, register.register));
        holder.registerName.setText(userPrettyPrintName(register.description));
        holder.registerValue.setText(rd.toString());
    }

    @Override
    public int getItemCount() {
        return registerList.size();
    }


}