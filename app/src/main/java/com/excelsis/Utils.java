package com.excelsis;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {

    public static final String PREFERENCE_FILE_KEY = "asdqwe756ddd";

    public static String getSharedPreferencesString(Context context, String id) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_FILE_KEY, Context.MODE_PRIVATE);
        return sp.getString(id, "");
    }

    public static void writeSharedPreferencesString(Context context, String id, String value) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_FILE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(id, value);
        editor.apply();
    }

    public static byte[] stringSmsToByteArray(String sms) {
        sms = sms.replace("[", "").replace("]", "");

        ArrayList<Byte> list = new ArrayList<>();
        for(String parts : sms.split(", ")) {
            if(parts.length() == 0) continue;
            list.add((byte) Integer.parseInt(parts, 16));
        }

        return toByteArray(list);
    }

    public static byte[] toByteArray(List<Byte> list) {
        if (list == null) {
            return null;
        }

        if (list.size() == 0) {
            return new byte[0];
        }

        final byte[] result = new byte[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public void addBytesToArrayList(Byte[] dataToAdd, ArrayList<Byte> arrayList) {
        arrayList.addAll(Arrays.asList(dataToAdd));
    }

    public static void addBytesToList(ArrayList<Byte> list, byte[] byteArray) {
        for (byte b : byteArray) {
            list.add(b);
        }
    }

    public static void addBytesToList(ArrayList<Byte> list, byte[] byteArray, int startAt) {
        for (int i=startAt; i<byteArray.length; i++) {
            list.add(byteArray[i]);
        }
    }

    public static int[] intListToArray(ArrayList<Integer> list) {
        int[] array = new int[list.size()];
        for (int i=0; i<array.length; i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static Byte[] toObject(byte[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new Byte[0];
        }
        final Byte[] result = new Byte[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    public static ArrayList<Byte> varToByteList(long n, int len) {
        ArrayList<Byte> list = new ArrayList<>();

        // init list with all zeros
        for (int i = 0; i < len; i++) {
            list.add((byte) 0);
        }

        for(int i=0; i<len; i++) {
            list.set(len-i-1, (byte)(n & 0xFF));
            n >>= 8;
        }

        return list;
    }

    // writes byte array in readable string format
    public static String arrayToString(byte[] array){
        if(array == null) return "[ null ]";
        if(array.length == 0) return "[]";

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        int i;
        for(i=0; i<array.length - 1; i++){
            sb.append(String.format("%02X", array[i]));
            sb.append(", ");
        }
        sb.append(String.format("%02X", array[i]));
        sb.append("]");

        return sb.toString();
    }


    public static String repeat(int count, String with) {
        return new String(new char[count]).replace("\0", with);
    }

    public static String repeat(int count) {
        return repeat(count, " ");
    }


    // writes byte array in readable string format
    public static String arrayToString(List<Byte> array){
        return arrayToString(array, ", ");
    }

    // writes byte array in readable string format
    public static String arrayToString(List<Byte> array, String seperator){
        if(array == null) return "[ null ]";

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        int i;
        for(i=0; i<array.size() - 1; i++){
            sb.append(String.format("%02X", array.get(i)));
            sb.append(seperator);
        }
        sb.append(String.format("%02X", array.get(i)));
        sb.append("]");

        return sb.toString();
    }


    // converts int array to byte array
    // this is because it won't allow to write value > 128 in byte array
    // it has to be first written in int[] than convert to byte[]
    public static byte[] intArrayToByteArray(int[] intArray){
        byte[] byteData = new byte[intArray.length];

        for(int i=0; i<byteData.length; i++){
            byteData[i] = (byte) intArray[i];
        }

        return byteData;
    }


    // end INCLUDING the bit at the end position
    public static int splitInt(int start, int end, int number) {
        int mask = (1 << (end-start+1)) - 1;
        return (number >> start) & mask;
    }


    public static byte[] byteListToArray(List<Byte> byteList) {
        byte[] byteArray = new byte[byteList.size()];

        for (int i = 0; i < byteArray.length; i++) {
            byteArray[i] = byteList.get(i);
        }

        return byteArray;
    }

    /*
    Converts integer value to byte array
     */
    public static Byte[] intToByteArray(int value, int len) {
        Byte[] b = new Byte[len];

        // TODO check if big or little indian is required
        // TODO check if the value is within limits
        for (int i = b.length - 1; i >= 0; i--) {
            b[i] = (byte) (value);
            value = value >>> 8;
        }

        return b;
    }

    public static String whatsWrongWithSerialNumber(String serialNumber) {
        if (!serialNumber.matches("[A-z][0-9]+")) {
            return "Wrong serial number format!";
        }

        return null;
    }

    public static String whatsWrongWithPhoneNumber(String phoneNumber) {
        if (!phoneNumber.matches("[0-9]+")) {
            return "Wrong phone number format!";
        }
        return null;
    }

    public static Set<String> encodeAlarmMask(String strValue) {
        Set<String> ss = new HashSet<>();
        int value = Integer.parseInt(strValue);
        for (int i = 1; i < 65535; i *= 2) {
            if (value / i % 2 == 1) ss.add(i + "");
        }
        return ss;
    }

    public static String decodeAlarmMask(Set<String> alarmMask) {
        int value = 0;
        String[] masks = alarmMask.toArray(new String[]{});
        for (String mask : masks) {
            value += Integer.parseInt(mask);
        }
        return value + "";
    }
}
