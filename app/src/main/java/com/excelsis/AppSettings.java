package com.excelsis;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import static com.excelsis.AppSettings.SettingsKey.REMEMBER_TEL_AND_SER_NUM;
import static com.excelsis.AppSettings.SettingsKey.SERIAL_NUM;
import static com.excelsis.AppSettings.SettingsKey.TELEPHONE_NUM;
import static com.excelsis.DevFTPServerSettings.FTPServerSettings.USE_FTP_SERVER;

public class AppSettings {
    Context context;

    public enum SettingsKey {
        SERIAL_NUM,
        TELEPHONE_NUM,
        REMEMBER_TEL_AND_SER_NUM
    }


    public AppSettings(Context context) {
        if(context == null) throw new NullPointerException();
        this.context = context;
    }

    private SharedPreferences getSP() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static String key(SettingsKey settingsKey) {
        return String.valueOf(settingsKey);
    }

    public String getTelephoneNum() {
        return getSP().getString(key(TELEPHONE_NUM), "");
    }

    public String getTelephoneNum(String defValue) {
        return getSP().getString(key(TELEPHONE_NUM), defValue);
    }

    public String getSerialNum() {
        return getSP().getString(key(SERIAL_NUM), "");
    }
    public boolean isRememberingValuesSet() { return getSP().getBoolean(key(REMEMBER_TEL_AND_SER_NUM), false); }

    public void saveTelAndSerial(String telephone, String serial) {
        SharedPreferences sp = getSP();
        SharedPreferences.Editor editor = sp.edit();

        editor.putString(key(SERIAL_NUM), serial);
        editor.putString(key(TELEPHONE_NUM), telephone);
        editor.apply();
    }

    public void saveBool(SettingsKey key, boolean value) {
        SharedPreferences sp = getSP();
        SharedPreferences.Editor editor = sp.edit();

        editor.putBoolean(key(key), value);
        editor.apply();
    }
}
