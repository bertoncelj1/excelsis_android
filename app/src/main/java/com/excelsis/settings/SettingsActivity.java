package com.excelsis.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.excelsis.AppSettings;
import com.excelsis.BarCodeFragment;
import com.excelsis.R;
import com.excelsis.Register;
import com.excelsis.RegisterLibrary;
import com.excelsis.Utils;
import com.excelsis.sms.creator.MessageComponents;
import com.excelsis.sms.creator.SmsMessageCreator;
import com.excelsis.sms.SmsSenderActivity;
import com.excelsis.sms.creator.bodyGenerators.TypeWriteBody;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity implements PreferenceFragmentCompat.OnPreferenceStartFragmentCallback  {

    private static final String DEBUG_TAG = "SettingsActivity";
    private static String ACTION_BAR_TITLE = "Settings";
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsMain())
                .commit();

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSendButtonClick();
            }
        });

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(ACTION_BAR_TITLE);
        }
    }

    private void handleSendButtonClick(){
        AppSettings appSettings = new AppSettings(this);
        String telephoneNumber = appSettings.getTelephoneNum();
        if(telephoneNumber == null ){
            Log.e(DEBUG_TAG, "Can't get telephone number");
            Toast.makeText(this, "Can't get telephone number", Toast.LENGTH_LONG).show();
        }

        // configure SmsMessageCreator
        SmsMessageCreator smsMessageCreator = new SmsMessageCreator(this);
        smsMessageCreator.ignoreMessageLength(false);
        smsMessageCreator.serialNumber = appSettings.getSerialNum();

        byte[][] byteMessages;
        MessageComponents.Messages messages;
        String messageComponents;
        try {
            ArrayList<Register> registers = RegisterLibrary.getSettingsRegisters().getRegistersArrayOrdered();
            TypeWriteBody body = new TypeWriteBody(this, registers);
            body.sendSameData(true);
            smsMessageCreator.setMessageBodyGenerator(body);

            messages = smsMessageCreator.getIndependentMessages();
            byteMessages = messages.getBytes();
            messageComponents = messages.getPrettyPrintMessage();
            Log.d(DEBUG_TAG, messageComponents);

            for(int i=0; i < byteMessages.length; i++){
                Log.d(DEBUG_TAG, "Message" + i + " :" + Utils.arrayToString(byteMessages[i]));
            }

            SmsSenderActivity.sendMessages(this, byteMessages, telephoneNumber, messageComponents);

        } catch (IllegalArgumentException e){
            Log.e(DEBUG_TAG, "Error composing message " + e.getMessage(), e);
            e.printStackTrace();
            Toast.makeText(this, "Error composing message " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                actionBar.setTitle(ACTION_BAR_TITLE);
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPreferenceStartFragment(PreferenceFragmentCompat caller, Preference pref) {
        // Instantiate the new Fragment
        final Fragment fragment = getSupportFragmentManager().getFragmentFactory().instantiate(
                getClassLoader(),
                pref.getFragment()
        );

        // set bundle and send all the info that the next fragment needs
        final Bundle args = pref.getExtras();
        Intent intent = pref.getIntent();
        if(intent != null) {
            Bundle intentBundle = pref.getIntent().getExtras();
            if (intentBundle != null)
                args.putAll(intentBundle); // put all extras from xml intent to the bundle
        }

        fragment.setArguments(args);
        fragment.setTargetFragment(caller, 0);

        // Replace the existing Fragment with the new Fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.settings, fragment)
                .addToBackStack(null)
                .commit();

        actionBar.setTitle(pref.getTitle());
        return true;
    }

}
