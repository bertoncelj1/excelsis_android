package com.excelsis.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.BaseAdapter;

import androidx.annotation.Nullable;

import com.excelsis.R;
import com.excelsis.Register;
import com.excelsis.RegisterLibrary;

import java.util.HashMap;
import java.util.logging.Logger;


public class SettingsPulse extends MyPreferenceFragment {

    private static final String DEBUG_TAG = "SettingsPulse";
    HashMap<String, Register> registers;

    private String INDEX_SP = "INDEX";
    private String INDEX_USER_SP = "INDEX_USER";
    private String SCALAR_USER_SP = "SCALAR_USER";
    private String SCALAR_SP = "SCALAR";
    private String ROLLOVER_SP = "ROLLOVER";
    private String P_ROLLOVER_SP = "P_ROLLOVER";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registers = RegisterLibrary.getSettingsRegisters().getRegisters();
    }


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.pulse_settings, rootKey);
        super.showHidePreferences();

        INDEX_SP += super.getPreferencesKeySuffix();
        INDEX_USER_SP += super.getPreferencesKeySuffix();
        SCALAR_USER_SP +=  super.getPreferencesKeySuffix();
        SCALAR_SP +=  super.getPreferencesKeySuffix();
        ROLLOVER_SP +=  super.getPreferencesKeySuffix();
        P_ROLLOVER_SP += super.getPreferencesKeySuffix();
    }



    private String calculateIndex(String indexUser, int pulse) {
        String num = indexUser;
        int decimalPosition = num.indexOf('.');
        if(decimalPosition == -1) decimalPosition = num.length();

        num += "0000000000";
        num = num.replace(".", "");
        int cutOff = decimalPosition - pulse;
        cutOff = Math.min(num.length(), cutOff);
        cutOff = Math.max(0, cutOff);

        return num.substring(0, cutOff);
    }

    /**
     * Calculates rollover from index input that user inserted
     */
    public static String calculateRollover(String indexUser) {
        String rollover = "";

        for(char c : indexUser.toCharArray()){
            rollover += c != '.' ? "9" : "";
        }

        return rollover;
    }

    public void updatePreferenceValues(String indexUserStr, double indexUser, int pulseExponentInt, String scalar) {
        SharedPreferences.Editor editor = sp.edit();

        String indexConverted = calculateIndex(indexUserStr, pulseExponentInt);
        editor.putString(INDEX_SP, indexConverted);
        editor.putString(ROLLOVER_SP, calculateRollover(indexConverted));
        editor.putString(P_ROLLOVER_SP, calculateRollover(indexConverted));
        editor.putString(SCALAR_SP, scalar);

        editor.commit();
        updatePreferenceSummaries();
    }

    @Override
    public void myOnSPChanged(SharedPreferences sharedPreferences, String preferenceKey) {
        try {
            String indexUserStr = sp.getString(INDEX_USER_SP, null);
            double indexUser = Double.parseDouble(indexUserStr);
            String scalarStr = sp.getString(SCALAR_USER_SP, null);
            scalarStr = convertScalar(scalarStr);
            int scalar = Integer.parseInt(scalarStr);
            int pulseExponentInt = getPulseExponentInt(scalar);

            updatePreferenceValues(indexUserStr, indexUser, pulseExponentInt, scalarStr);
        } catch (NumberFormatException e) {
            Log.e(DEBUG_TAG, "Unable to read all the preferences. " + e.getMessage());
            e.printStackTrace();
        }
    }

    private int getPulseExponentInt(int scalar) {
        switch (scalar) {
            case -10000: return -4;
            case -1000: return -3;
            case -100: return -2;
            case -10: return -1;
            case 1: return 0;
            case 10: return 1;
            case 100: return 2;
            case 1000: return 3;
            case 10000: return 4;

            default:
                String msg = "Unable to parse scalar: " + scalar;
                Log.e(DEBUG_TAG, msg);
                throw new NumberFormatException(msg);
        }
    }

    private String convertScalar(String scalar) {
        switch (scalar) {
            case "0.0001": return "-10000";
            case "0.001": return "-1000";
            case "0.01": return "-100";
            case "0.1": return "-10";
            case "1": return scalar;
            case "10": return scalar;
            case "100": return scalar;
            case "1000": return scalar;

            default:
                return scalar;
        }
    }
}
