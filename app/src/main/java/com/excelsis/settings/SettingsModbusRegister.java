package com.excelsis.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.preference.Preference;

import com.excelsis.R;
import com.excelsis.RegisterLibrary;


public class SettingsModbusRegister extends MyPreferenceFragment {

    private static final String DEBUG_TAG = "SettingsModbusRegister";
    private String REG_FORMAT = "REG_FORMAT";
    private String REG_FORMAT_USER = "REG_FORMAT_USER";
    private String REG_LOG_FORMAT = "REG_LOG_FORMAT";


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.modbus_register_settings, rootKey);
        super.showHidePreferences();

        REG_FORMAT_USER += super.getPreferencesKeySuffix();
        REG_FORMAT += super.getPreferencesKeySuffix();
        REG_LOG_FORMAT += super.getPreferencesKeySuffix();
    }

    @Override
    public void myOnSPChanged(SharedPreferences sharedPreferences, String preferenceKey) {
        try {
            if (preferenceKey.equals(REG_FORMAT_USER)) {
                SharedPreferences.Editor editor = sp.edit();

                String fmt = sp.getString(REG_FORMAT_USER, null);
                String fmtConverted = convertFormat(fmt);
                editor.putString(REG_FORMAT, fmtConverted);
                editor.putString(REG_LOG_FORMAT, fmtConverted);

                editor.commit();
                updatePreferenceSummaries();
            }
        } catch (NumberFormatException e) {
            Log.e(DEBUG_TAG, "Unable to read all the preferences. " + e.getMessage());
            e.printStackTrace();
        }
    }

    private String convertFormat(String format) {
        switch (format) {
            case "none": return "0";
            case "int16": return "18";
            case "uint16": return "2";
            case "int32": return "20";
            case "uint32": return "4";
            case "int64": return "24";
            case "uint64": return "8";
            case "float32": return "68";
            case "float64": return "136";
            case "totaltype": return "248";

            default:
                return "0";
        }
    }
}
