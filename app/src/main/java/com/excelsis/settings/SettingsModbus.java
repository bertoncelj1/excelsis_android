package com.excelsis.settings;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.preference.Preference;

import com.excelsis.R;


public class SettingsModbus extends MyPreferenceFragment {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.modbus_settings, rootKey);
        this.showHidePreferences();
    }

}
