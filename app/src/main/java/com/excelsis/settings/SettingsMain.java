package com.excelsis.settings;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.preference.Preference;

import com.excelsis.R;


public class SettingsMain extends MyPreferenceFragment {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.main_settings, rootKey);
        this.showHidePreferences();
    }

}
