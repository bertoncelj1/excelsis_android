package com.excelsis.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.MultiSelectListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceGroup;

import com.excelsis.R;
import com.excelsis.Register;
import com.excelsis.RegisterLibrary;
import com.excelsis.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

public abstract class MyPreferenceFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String DEBUG_TAG = "MyPreferenceFragment";
    int index = -1;
    String prefIndex;

    private ArrayList<Preference> preferences = null;
    private HashMap<String, Preference> preferencesMap = null;

    public String getPreferencesKeySuffix() {
        if(prefIndex == null)
            prefIndex = calcPreferencesIndex();

        return prefIndex;
    }

    public String calcPreferencesIndex() {
        Bundle bundle = getArguments();
        if(bundle == null) return "";
        index = bundle.getInt("index", -1);
        if(index == -1) {
            // if index variable doesn't exist just return no suffix
            return "";
        }

        return "_" + index;
    }

    public HashMap<String, Preference> getPreferencesMap() {
        if(preferencesMap == null) {
            preferencesMap = new HashMap<>();
            for (Preference preference : getAllPreferences()) {
                preferencesMap.put(preference.getKey(), preference);
                preference.setSummaryProvider(null);
            }
        }

        return preferencesMap;
    }

    public void updatePreferenceSummaries() {
        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();

        for(Preference preference: getAllPreferences()) {
            if(preference instanceof  EditTextPreference || preference instanceof ListPreference
                    || preference instanceof MultiSelectListPreference) {
                preference.setSummaryProvider(null);
                preference.setSummary(getSummary(sp, preference.getKey()));
            }
        }
    }

    public String getSummary(SharedPreferences sp, String preferenceKey) {
        try {
            String summary = "";
            if (preferenceKey.equals("ALARM_MASK_E2")
                    || preferenceKey.equals("ALARM_MASK_E4")
                    || preferenceKey.equals("ALARM_MASK_E8")) {
                Set<String> flags = sp.getStringSet (preferenceKey, new HashSet<String>());
                summary = Utils.decodeAlarmMask(flags);
            }
            else {
                summary = sp.getString(preferenceKey, "");
            }
            return summary.length() == 0 ? "value not set" : summary;
        } catch (ClassCastException ex) {
            return "??? " + preferenceKey;
        }
    }

    public void updateSummary(SharedPreferences sp, String preferenceKey) {
        Preference preference = getPreferencesMap().get(preferenceKey);
        if(preference == null) return;

        preference.setSummary(getSummary(sp, preferenceKey));
    }

    public void updateSummary(String preferenceKey) {
        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
        updateSummary(sp, preferenceKey);
    }

    public void updatePreferenceText(SharedPreferences sp, String preferenceKey, String value) {
        Preference preference = getPreferencesMap().get(preferenceKey);
        if(preference == null) return;

        preference.setSummary(value);
        if(preference instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) preference;
            etp.setText(value);
        }
    }

    public void updatePreferenceAlarm(SharedPreferences sp, String preferenceKey, String value) {
        Preference preference = getPreferencesMap().get(preferenceKey);
        if(preference == null) return;

        preference.setSummary(value);
        MultiSelectListPreference mlist = (MultiSelectListPreference) preference;
        mlist.setValues(Utils.encodeAlarmMask(value));
    }

    /**
     * Go through all the preference currently visible on the screen and just show
     * hide those that are defined in the registers file
     */
    public void showHidePreferences() {
        HashMap<String, Register> registers = RegisterLibrary.getSettingsRegisters().getRegisters();

        for(Preference preference: getAllPreferences()) {
            String preferenceKey = preference.getKey();
            if(preferenceKey == null) {
                preference.setVisible(true);
                continue; // just ignore this preference if the key is not set
            }

            if(sp != null) sp.unregisterOnSharedPreferenceChangeListener(this);
            Register register = registers.get(preferenceKey);
            if(register == null) {
                preference.setVisible(false); // disable the preference if the register is not set inside the library
            } else {
                preference.setVisible(true);
                // only set value if the register is an actual register and not just a "SHOW something something" "register"
                if(register.isAnActualRegister()) {

                    // if we change preference key like we did on the addIndexToPreferences
                    // we have to set default value manually
                    //setPreferenceDefaultValue(preference, register.defaultValue);


                    preference.setDefaultValue(register.defaultValue);
                }
            }
            if(sp != null) sp.registerOnSharedPreferenceChangeListener(this);
        }
    }


    private void setPreferenceDefaultValue(Preference preference, String value) {
        if(preference instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) preference;
            etp.setText(value);
        } else if(preference instanceof ListPreference) {
            ListPreference lp = (ListPreference) preference;
            lp.setValue(value);
        } else {
            Log.e(DEBUG_TAG, "Unable to set default value for preference with class: " + preference.getClass() + ", " + preference.getKey());
        }
    }

    public ArrayList<Preference> collectAllPreferences(PreferenceGroup pg) {
        ArrayList<Preference> preferences = new ArrayList<>(pg.getPreferenceCount());
        for(int i=0; i < pg.getPreferenceCount(); i++) {
            Preference preference = pg.getPreference(i);
            if(preference instanceof PreferenceGroup) {
                preferences.addAll(collectAllPreferences((PreferenceGroup) preference));
            }
            String suffix = getPreferencesKeySuffix();
            String prefKey = preference.getKey();
            if(prefKey != null) {
                preference.setKey(prefKey + suffix);
                preferences.add(preference);
            }
        }

        return preferences;
    }


    public ArrayList<Preference> getAllPreferences() {
        if(this.preferences == null) {
            PreferenceGroup pg = getPreferenceManager().getPreferenceScreen();
            this.preferences = collectAllPreferences(pg);
        }

        return preferences;
    }

    SharedPreferences sp;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getPreferenceScreen().getSharedPreferences();
        updatePreferenceSummaries();
    }


    @Override
    public void onResume() {
        super.onResume();
        this.showHidePreferences();
        updatePreferenceSummaries();
        sp.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        sp.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String preferenceKey) {
        updateSummary(sharedPreferences, preferenceKey);
        myOnSPChanged(sharedPreferences, preferenceKey);
    }

    public void myOnSPChanged(SharedPreferences sharedPreferences, String preferenceKey) {
        // Get the target alarm mask for type
        String alarmMaskType = "";
        if (sp.contains("ALARM_MASK_E8") == true)
        {
            alarmMaskType = "ALARM_MASK_E8";
        }
        else if (sp.contains("ALARM_MASK_E4") == true)
        {
            alarmMaskType = "ALARM_MASK_E4";
        }
        else if (sp.contains("ALARM_MASK_E2") == true)
        {
            alarmMaskType = "ALARM_MASK_E2";
        }
        else
        {
            return;
        }

        // Update Z Delay
        if (preferenceKey.equals("Z_DELAY"))
        {
            updateAlarm(sharedPreferences, alarmMaskType, "Z_DELAY", 64);
        }

        // Update V level
        else if (preferenceKey.equals("V_LEVEL"))
        {
            updateAlarm(sharedPreferences, alarmMaskType, "V_LEVEL", 1);
        }
    }

    private void updateAlarm(SharedPreferences sharedPreferences, String alarmType, String changedKey, Integer flagValue)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> flags = sharedPreferences.getStringSet (alarmType, new HashSet<String>());

        String val = sharedPreferences.getString(changedKey, null);
        if (val.equals("0"))
        {
            flags.remove(flagValue + "");
            editor.putStringSet(alarmType, flags);
        }
        else if (val.length() > 0)
        {
            if (!flags.contains(1 + ""))
            {
                flags.add(flagValue + "");
                editor.putStringSet(alarmType, flags);
            }
        }
        editor.commit();
        updatePreferenceAlarm(sharedPreferences, alarmType, Utils.decodeAlarmMask(flags));
    }
}
