package com.excelsis.sms;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.excelsis.R;
import com.excelsis.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by anze on 10/9/16.
 */
public class SmsSenderActivity extends Activity implements View.OnClickListener {

    private static final String DEBUG_TAG = "SmsSenderActivity";

    private static final int MAX_SMS_MESSAGE_LENGTH = 150;
    private static final String SMS_DELIVERED = "SMS_DELIVERED";
    private static final String SMS_SENT = "SMS_SENT";

    private TextView mSmsStatus;
    private ProgressBar mProgressBar;
    private Button mSendSms;
    private TextView mSmsInfo;

    private String mPhoneNumber;
    private byte[][] mBinaryMessages;
    private String mMessageComponents;

    private int smsPort;

    private int currentSmsMessage = 0;
    private String[] messageStates;

    public static void sendMessages(Context context, byte[][] binaryMessages, String telephoneNumber, String messageComponents){
        // start send message activity
        Intent sendMessage = new Intent(context, SmsSenderActivity.class);
        sendMessage.putExtra("binaryMessages", binaryMessages);
        sendMessage.putExtra("phoneNumber", telephoneNumber);
        sendMessage.putExtra("messageComponents", messageComponents);
        context.startActivity(sendMessage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_sender);

        // get binary message and phone number from bundle
        Bundle bundle = this.getIntent().getExtras();
        if(bundle != null){
            Object[] binMsg = (Object[]) bundle.getSerializable("binaryMessages");
            // sending 2d array, means that we need cast each array separately
            if(binMsg != null) {
                mBinaryMessages = new byte[binMsg.length][];
                for(int i = 0; i < binMsg.length; i++) mBinaryMessages[i] = (byte[]) binMsg[i];
            } else {
                mBinaryMessages = null;
            }

            mPhoneNumber = bundle.getString("phoneNumber");
            mMessageComponents = bundle.getString("messageComponents");
        }

        if(mBinaryMessages == null || mPhoneNumber == null){
            Log.e(DEBUG_TAG, "Unable to get message or phone number. Exiting SmsSenderActivity");
            finish();
            return;
        }

        mSmsInfo = findViewById(R.id.smsInfo);
        printSmsInfo();

        mSendSms = findViewById(R.id.sendSmsButton);
        mSendSms.setOnClickListener(this);
        mSmsStatus = findViewById(R.id.smsSendStatus);
        mProgressBar = findViewById(R.id.smsSendProgressBar);
        mProgressBar.setVisibility(View.INVISIBLE);

        ((TextView) findViewById(R.id.messageContents)).setText(mMessageComponents);

        registerReceiver(sendReceiver, new IntentFilter(SMS_SENT));
        registerReceiver(deliveredReceiver, new IntentFilter(SMS_DELIVERED));

        // load sms port from the resources
        smsPort = getResources().getInteger(R.integer.sms_port);

        Log.d(DEBUG_TAG, "Phone number: " + mPhoneNumber);
        for(int i=0; i<mBinaryMessages.length; i++){
            Log.d(DEBUG_TAG, "Message" + i + ":" + Utils.arrayToString(mBinaryMessages[i]));
        }
    }

    private void printSmsInfo() {
        int nMsg = mBinaryMessages.length;

        String msgLengths = "";
        for(int i=0; i<nMsg; i++){
            msgLengths += mBinaryMessages[i].length + " bytes";
            if(i < nMsg -1) msgLengths += i < nMsg - 2? ", " : " and ";
        }

        mSmsInfo.setText(String.format(Locale.getDefault(),
                "%d message%s (%s)\nto %s", nMsg, nMsg==1? "":"s", msgLengths, mPhoneNumber));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(sendReceiver);
        unregisterReceiver(deliveredReceiver);
    }

    private void setSmsStatus(int msgNumber, String status){
        Log.d(DEBUG_TAG, status);
        messageStates[msgNumber] = status;

        StringBuilder statusText = new StringBuilder();
        for(int i=0; i<messageStates.length; i++) {
            statusText.append("Msg ").append(i+1).append(": ").append(messageStates[i]).append("\n");
        }
        mSmsStatus.setText(statusText.toString());
        Toast.makeText(SmsSenderActivity.this, status, Toast.LENGTH_SHORT).show();
    }

    private BroadcastReceiver sendReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String info = "";
            int msgNumber = intent.getExtras().getInt("num");

            switch(getResultCode()) {
                case Activity.RESULT_OK: info += "Sent"; break; // Waiting for message to be delivered
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE: info += "Send failed, generic failure"; break;
                case SmsManager.RESULT_ERROR_NO_SERVICE: info += "Send failed, no service"; break;
                case SmsManager.RESULT_ERROR_NULL_PDU: info += "Send failed, null pdu"; break;
                case SmsManager.RESULT_ERROR_RADIO_OFF: info += "Send failed, radio is off"; break;
            }
            Log.i(DEBUG_TAG, "On message" + currentSmsMessage + " received: " + info);

            if(getResultCode() != RESULT_OK){
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            setSmsStatus(msgNumber, info);
            sendNextBinaryMessage();
        }
    };

    private BroadcastReceiver deliveredReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String info = "";
            int msgNumber = intent.getExtras().getInt("num");

            switch(getResultCode()) {
                case Activity.RESULT_OK: info += "Delivered."; break; // Message delivered. Waiting for reply
                case Activity.RESULT_CANCELED: info += "Message not delivered"; break;
            }

            mProgressBar.setVisibility(View.INVISIBLE);
            Log.i(DEBUG_TAG, "On message delivered: " + info);
            setSmsStatus(msgNumber, info);
        }
    };



    public void sendBinarySms(int msgNumber, String phoneNumber, byte[] binaryData){
        SmsManager manager = SmsManager.getDefault();

        // the idea is that when getting Pending intent if you pass the same request code
        // it can return the same Pending intent. So if we change an intent we also have to change
        // this randomReqCode to make sure that the new Intent will be actually used and not the old one
        int randomReqCode = 12345;
        PendingIntent piSend = PendingIntent.getBroadcast(
                this,
                msgNumber + randomReqCode,
                new Intent(SMS_SENT).putExtra("num", msgNumber),
                0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(
                this,
                msgNumber + randomReqCode,
                new Intent(SMS_DELIVERED).putExtra("num", msgNumber),
                0);

        if(binaryData.length >= MAX_SMS_MESSAGE_LENGTH) {
            Log.e(DEBUG_TAG, "Message is too long." +
                    "msgLen:" + binaryData.length + " maxLen:" + MAX_SMS_MESSAGE_LENGTH);
            throw new IllegalArgumentException("Message is too long.");
            // binaryData = Arrays.copyOfRange(binaryData, 0 , MAX_SMS_MESSAGE_LENGTH);
        }

        manager.sendDataMessage(phoneNumber, null, (short) smsPort, binaryData, piSend, piDelivered);
    }

    private boolean sendNextBinaryMessage() {
        if(currentSmsMessage == -1) {
            messageStates = new String[mBinaryMessages.length];
            Arrays.fill(messageStates, "...");
        }

        currentSmsMessage ++;
        if(currentSmsMessage < mBinaryMessages.length) {
            Log.i(DEBUG_TAG, "Sending message " + currentSmsMessage);
            setSmsStatus(currentSmsMessage, "Sending ...");
            sendBinarySms(currentSmsMessage, mPhoneNumber, mBinaryMessages[currentSmsMessage]);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
       trySendingSms();
    }

    private boolean pendingSmsSend = false;
    private void trySendingSms() {
        if(!hasSMSPermission()){
            pendingSmsSend = true;
            getSmsPermission();
        } else {
            pendingSmsSend = false;
            mSmsInfo.setVisibility(View.GONE);
            mSendSms.setVisibility(View.GONE);

            //mProgressBar.setVisibility(View.VISIBLE);

            currentSmsMessage = -1;
            boolean isSend = sendNextBinaryMessage();

            if(!isSend) Log.e(DEBUG_TAG, "Not even the first message was send!");
        }
    }

    private boolean hasSMSPermission(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    private static final int SMS_PERMISSION_REQUEST_CODE = 123;
    private void getSmsPermission(){
        final String[] permissions = {
                "android.permission.RECEIVE_SMS",
                "android.permission.READ_SMS",
                "android.permission.SEND_SMS"
        };

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1){
            requestPermissions(permissions, SMS_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults){
        switch(permsRequestCode){
            case SMS_PERMISSION_REQUEST_CODE:
                boolean permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if(pendingSmsSend) {
                    if(permissionGranted) {
                        trySendingSms();
                    }else{
                        Toast.makeText(this, "App doesn't have permissions to send an SMS",
                                Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }

    }
}
