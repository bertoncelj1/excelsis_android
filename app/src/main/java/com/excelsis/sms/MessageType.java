package com.excelsis.sms;

public enum MessageType {
    REGISTER_READ(0x01),
    REGISTER_READ_RESPONSE(0x02),
    REGISTER_WRITE(0x03),
    REGISTER_WRITE_ANDROID(0x13), // write command iz android aplikacije
    REGISTER_WRITE_ANDROID_END(0x14), // write command iz android aplikacije. Oznaka za končni zadnji sms
    CLOCK_SYNC_REQUEST(0x04),
    ACKNOWLEDGE(0x05),
    ANDROID_MACRO(0x16),
    LOG_DATA(0x30),
    GPRS_FIRMWARE_UPDATE(0x7F),
    UNKNOWN(-1);

    private final int id;

    MessageType(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }

    // returns a message type from a given int
    public static MessageType fromInt(int a) {
        for (MessageType mt : MessageType.values()) {
            if (mt.id == a) return mt;
        }
        return MessageType.UNKNOWN;
    }
}
