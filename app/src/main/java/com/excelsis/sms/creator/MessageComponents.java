package com.excelsis.sms.creator;

import com.excelsis.Register;
import com.excelsis.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MessageComponents {


    /**
     * Each part of the message is a messagePart, headers, registers ....
     * <p>
     * Note also that each MessagePart also has description variable that describes what is
     * saved in the message. That's so that we can pretty print message.
     */
    public static class MessagePart {
        String description;
        // parts that this message is made of
        ArrayList<MessagePart> messageParts = new ArrayList<>();
        // raw data that is saved in this message
        List<Byte> data;

        public MessagePart(String description) {
            this(new ArrayList<Byte>(), description);
        }

        public MessagePart(List<Byte> data, String description) {
            this.data = data;
            this.description = description;
        }

        public int length() {
            int length = 0;

            for (int i = 0; i < messageParts.size(); i++) {
                length += messageParts.get(i).length();
            }

            if (data != null) length += data.size();

            return length;
        }


        public ArrayList<Byte> getData() {
            ArrayList<Byte> data = new ArrayList<>(this.data);

            for (int i = 0; i < messageParts.size(); i++) {
                data.addAll(messageParts.get(i).getData());
            }

            return data;
        }


        public String getPrettyPrintMessage() {
            return getPrettyPrintMessage(4);
        }

        public String getPrettyPrintMessage(int indent) {
            return getPrettyPrintMessage(indent, 0);
        }

        private String getPrettyPrintMessage(int indent, int level) {
            StringBuilder s = new StringBuilder(Utils.repeat(indent * level, " "));
            if (data.size() > 0) {
                s.append(Utils.arrayToString(data, " "));
                s.append(" ");
            }

            s.append(description).append("\n");

            for (int i = 0; i < messageParts.size(); i++) {
                s.append(messageParts.get(i).getPrettyPrintMessage(indent, level + 1));
            }

            // Separate each message by new line so the message is prettier
            if(this instanceof Message) s.append("\n");

            return s.toString();
        }

        public void addData(Byte data, String description) {
            ArrayList<Byte> al = new ArrayList<>();
            al.add(data);
            this.addData(al, description);
        }

        public void addData(List<Byte> data, String description) {
            MessagePart m = new MessagePart(data, description);
            messageParts.add(m);
        }

        public void addData(List<Byte> data, Register register) {
            String regString = String.format(Locale.ENGLISH, "%s(%d): '%s'",
                    register.preferenceKey, register.dataType, register.value);

            this.addData(data, regString);
        }

        void addMessagePart(MessagePart messagePart) {
            messageParts.add(messagePart);
        }

    }

    public static class Message extends MessagePart {

        public Message(String description) {
            super(description);
        }
    }

    public static class Messages extends MessagePart {

        public Messages() {
            super("Messages: ");
        }

        /**
         * Return raw bytes for all the messages
         */
        public byte[][] getBytes() {
            byte[][] rawBytes = new byte[size()][];
            for(int i=0; i<size(); i++) {
                rawBytes[i] = Utils.byteListToArray(messageParts.get(i).getData());
            }
            return rawBytes;
        }

        void add(Message message) {
            messageParts.add(message);
        }

        public int size() {
            return messageParts.size();
        }
    }
}
