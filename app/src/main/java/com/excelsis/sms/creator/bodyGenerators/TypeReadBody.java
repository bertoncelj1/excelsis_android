package com.excelsis.sms.creator.bodyGenerators;

import com.excelsis.Register;
import com.excelsis.sms.MessageType;
import com.excelsis.sms.creator.MessageComponents;
import com.excelsis.sms.creator.SmsMessageCreator;

import java.util.List;

public class TypeReadBody extends BodyGenerator {

    private static final String DEBUG_TAG = "TypeReadBody";

    @Override
    public MessageType getMessageType() {
        return MessageType.REGISTER_READ;
    }

    @Override
    public MessageComponents.MessagePart getBody() {
        MessageComponents.MessagePart registers = new MessageComponents.MessagePart("Body: registers");

        for(int i=0; i<mRegistersToSend.size(); i++) {
            Register register = mRegistersToSend.get(i);

            // for read registers we just need to send register address
            List<Byte> registerAddress = composeRegisterAddress(register);
            registers.addData(registerAddress, register.toStringDescription());
        }

        return registers;
    }
}
