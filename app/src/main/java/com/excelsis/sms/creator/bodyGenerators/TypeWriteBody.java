package com.excelsis.sms.creator.bodyGenerators;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.excelsis.Register;
import com.excelsis.Utils;
import com.excelsis.sms.MessageType;
import com.excelsis.sms.creator.MessageComponents;
import com.excelsis.sms.creator.SmsMessageCreator;
import com.excelsis.sms.decoder.typeEncoders.Type12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TypeWriteBody extends BodyGenerator {

    private static final String DEBUG_TAG = "TypeWriteBody";
    private SharedPreferences sp;

    @Override
    public MessageType getMessageType() {
        return MessageType.REGISTER_WRITE_ANDROID;
    }

    @Override
    public MessageType getMessageTypeEnd() {
        return MessageType.REGISTER_WRITE_ANDROID_END;
    }

    public TypeWriteBody(Context context, List<Register> registersToSend) {
        this.setRegistersToSend(registersToSend);
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public MessageComponents.MessagePart getBody() {

        MessageComponents.MessagePart registers = new MessageComponents.MessagePart("");
        for (Register register : mRegistersToSend) {

            String userValue;
            try {
                if(register.preferenceKey.equals("ALARM_MASK_E2")
                        || register.preferenceKey.equals("ALARM_MASK_E4")
                        || register.preferenceKey.equals("ALARM_MASK_E8")) {
                    // alarm mask is a special case and we can't just pass string to the preferences
                    // we need to convert it first to an set string.
                    userValue = Utils.decodeAlarmMask(sp.getStringSet(register.preferenceKey, null));
                } else {
                    userValue = sp.getString(register.preferenceKey, null);
                }
            } catch (ClassCastException ex) {
                Log.e(DEBUG_TAG, "Unable to read preference key " + register.preferenceKey + " ignoring ... " + ex.getCause());
                continue;
            }

            // if uservalue is null it means that the user didn't even see the field yet
            if (userValue == null) {
                userValue = register.defaultValue;
            }

            if (!register.isAnActualRegister()) continue;

            if (userValue.equals("")) {
                Log.i(DEBUG_TAG, "User removed the value. Don't know what to send, so I am not sending this register" +
                        ". Register: " + register.toString());
                continue;
            }

            // values are the same, nothing to send
            if (!mSendSameData && register.defaultValue.equals(userValue)) {
                Log.i(DEBUG_TAG, "Default value and new value are the same. Skipping. Register: " + register.toString());
                continue;
            } else {
                Log.d(DEBUG_TAG, String.format(
                        "Default value and new value are not the same! " +
                                "sendSameData: %s, Register: %s, UserValue: '%s', Register.defaultValue: '%s'",
                        mSendSameData, register.toString(), userValue, register.defaultValue));
            }

            // set userValue to value to make code more readable
            register.value = userValue;

            try {
                // converts to hex data suitable for SMS
                final List<Byte> convertedRegisterValue = composeSettingRegister(register);

                // appends to message aka. dataToSend
                registers.addData(convertedRegisterValue, register);
            } catch (Exception e) {
                Log.e(DEBUG_TAG, "Error encoding register " + register + " " + e.getMessage(), e);
                e.printStackTrace();
                throw new IllegalArgumentException("Error encoding register " + register + " " + e.getMessage());
            }
        }

        return registers;
    }

    private List<Byte> composeSettingRegister(Register register) {
        List<Byte> hexData = new ArrayList<>();

        // add register address
        hexData.addAll(composeRegisterAddress(register));

        // converts value depending on type
        Byte[] hexValue;
        switch (register.dataType) {
            case 128:
                hexValue = type_128(register);
                break;
            case 44:
                hexValue = type_44(register);
                break;
            case 29:
                hexValue = type_29(register);
                break;
            case 21:
                hexValue = type_21(register);
                break;
            case 19:
                hexValue = type_19(register);
                break;
            case 16:
                hexValue = type_16(register);
                break;
            case 12:
                hexValue = type_12(register);
                break;
            case 1:
                hexValue = type_1(register);
                break;
            case 2:
                hexValue = type_2(register);
                break;
            case 3:
                hexValue = type_3(register);
                break;
            case 4:
                hexValue = type_4(register);
                break;
            case 9:
                hexValue = type_9(register);
                break;

            default:
                Log.e(DEBUG_TAG, "Unrecognized data type " + register.dataType);
                hexValue = new Byte[0];
                throw new IllegalArgumentException("Unrecognized data type " + register.dataType);
        }
        hexData.addAll(Arrays.asList(hexValue));

        return hexData;
    }

    private Byte[] type_128(Register register) {
        int len;  //max len is 128
        len = register.value.length();

        if (len > 128)
            throw new IllegalArgumentException("String longer than 128. String length: " + len);

        Byte[] b = new Byte[len + 1];
        b[0] = (byte) len;
        for (int i = 0; i < len; i++) {
            b[i + 1] = (byte) register.value.charAt(i);
        }

        return b;
    }

    /**
     * "Float type"
     */
    private Byte[] type_44(Register register) {
        final float num = Float.parseFloat(register.value.replace(',', '.'));
        int intBits =  Float.floatToIntBits(num);
        return new Byte[] {
                (byte) (intBits >> 24),
                (byte) (intBits >> 16),
                (byte) (intBits >> 8),
                (byte) (intBits)
        };
    }


    private Byte[] type_29(Register register) {
        double num = Double.parseDouble(register.value);
        int wNum; // number to be Written

        // get multiplier
        if (num == 1) wNum = 1;
        else if (num == 10) wNum = -10;
        else if (num == 100) wNum = -100;
        else if (num == 1000) wNum = -1000;
        else if (num == 0.1) wNum = 10;
        else if (num == 0.01) wNum = 100;
        else if (num == 0.001) wNum = 1000;
        else if (num == 0.0001) wNum = 10000;
        else
            throw new IllegalArgumentException("Number must be of form 10^a, where a is a whole number.");

        return Utils.intToByteArray(wNum, 2);
    }

    private Byte[] type_22(Register register) {
        // TODO this is not correct transformation it needs to be "multiplication and divide"
        final int num = Integer.parseInt(register.value);
        return Utils.intToByteArray(num, 2);
    }

//    private Byte[] type_21(Register register) {
//        String[] parts = register.value.split(":");
//
//        if (parts.length < 2)
//            throw new IllegalArgumentException("Incorrect date provided: " + register.value);
//
//        int hour = Integer.parseInt(parts[0]);
//        int minutes = Integer.parseInt(parts[1]);
//
//        if (hour < 0 || hour > 23)
//            throw new IllegalArgumentException("Hours are out of bounds: " + hour);
//        if (minutes < 0 || minutes > 59)
//            throw new IllegalArgumentException("Minutes is out of bounds: " + minutes);
//
//        return new Byte[]{
//                (byte) (hour),
//                (byte) (minutes)
//        };
//    }

    private Byte[] type_21(Register register) {
        int len;  //max len is 21
        len = register.value.length();

        if (len > 21)
            throw new IllegalArgumentException("String longer than 21. String length: " + len);

        Byte[] b = new Byte[len + 1];
        b[0] = (byte) len;
        for (int i = 0; i < len; i++) {
            b[i + 1] = (byte) register.value.charAt(i);
        }

        return b;
    }

    private Byte[] type_19(Register register) {
        int len;  //max len is 9
        len = register.value.length();

        if (len > 9)
            throw new IllegalArgumentException("String longer than 9. String length: " + len);

        Byte[] b = new Byte[len + 1];
        b[0] = (byte) len;
        for (int i = 0; i < len; i++) {
            b[i + 1] = (byte) register.value.charAt(i);
        }

        return b;
    }

    // string with max length of 9
    private Byte[] type_9(Register register) {
        int len = register.value.length();

        if (len > 9)
            throw new IllegalArgumentException("String longer than 9. String length: " + len);

        Byte[] b = new Byte[len + 1];
        b[0] = (byte) len;
        for (int i = 0; i < len; i++) {
            b[i + 1] = (byte) register.value.charAt(i);
        }

        return b;
    }

    // string with max length 16
    private Byte[] type_16(Register register) {
        int len; //max len is 16
        len = register.value.length();

        if (len > 16)
            throw new IllegalArgumentException("String longer than 16. String length: " + len);

        Byte[] b = new Byte[len + 1];
        b[0] = (byte) len;
        for (int i = 0; i < len; i++) {
            b[i + 1] = (byte) register.value.charAt(i);
        }

        return b;
    }

    private Byte[] type_12(Register register) {
        return new Type12().encode(register);
    }

    private Byte[] type_4(Register register) {
        final int num = Integer.parseInt(register.value);
        return Utils.intToByteArray(num, 4);
    }

    private Byte[] type_3(Register register) {
        String[] parts = register.value.split(":");

        if (parts.length < 3)
            throw new IllegalArgumentException("Incorrect date provided: " + register.value);

        int year = Integer.parseInt(parts[0]);
        int month = Integer.parseInt(parts[1]);
        int day = Integer.parseInt(parts[2]);

        if (year < 0 || year > 99)
            throw new IllegalArgumentException("Year is out of bounds: " + year);
        if (month < 1 || month > 12)
            throw new IllegalArgumentException("Month is out of bounds: " + month);
        if (day < 1 || year > 31)
            throw new IllegalArgumentException("Day is out of bounds: " + day);

        return new Byte[]{
                (byte) (year),
                (byte) (month),
                (byte) (day)
        };
    }

    private Byte[] type_2(Register register) {
        final int num = Integer.parseInt(register.value);
        return Utils.intToByteArray(num, 2);
    }

    private Byte[] type_1(Register register) {
        int num = Integer.parseInt(register.value);

        if ((num & 0xFF) != num) {
            // TODO expand error message, create log.e
            throw new IllegalArgumentException("Number is over 1 byte");
        }

        return new Byte[]{(byte) num};
    }



}
