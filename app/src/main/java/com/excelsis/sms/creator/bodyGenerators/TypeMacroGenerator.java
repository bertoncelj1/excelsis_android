package com.excelsis.sms.creator.bodyGenerators;

import com.excelsis.sms.MessageType;

import static com.excelsis.sms.creator.MessageComponents.*;

public class TypeMacroGenerator extends BodyGenerator{

    private final byte password;
    private final Command command;

    public enum Command {
        TRIGGER_MB(0x60),
        RESET_SMS(0x4A);

        private final int id;

        Command(int id) {
            this.id = id;
        }

        public byte getValue() {
            return (byte) id;
        }
    }

    public TypeMacroGenerator(Command command, byte password) {
        this.command = command;
        this.password = password;
    }

    @Override
    public MessagePart getBody() {
        MessagePart data = new MessagePart("Body: trigger");

        data.addData(command.getValue(), "command: " + command.name());
        data.addData(password, "password");

        return data;
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.ANDROID_MACRO;
    }
}
