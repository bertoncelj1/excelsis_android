package com.excelsis.sms.creator.bodyGenerators;

import android.content.Context;

import com.excelsis.Register;
import com.excelsis.sms.MessageType;
import com.excelsis.sms.creator.MessageComponents;

import java.util.ArrayList;
import java.util.List;

public abstract class BodyGenerator {

    protected final ArrayList<Register> mRegistersToSend = new ArrayList<>();
    /*
   Send data even if user hasn't changed anything
    */
    protected boolean mSendSameData = true;

    protected List<Byte> composeRegisterAddress(Register register) {
        List<Byte> registerAddress = new ArrayList<>();

        // adds channel and register to the beginning of value
        registerAddress.add((byte) register.channel);
        registerAddress.add((byte) ((byte) (register.register >> 8) & 0xFF));
        registerAddress.add((byte) ((byte) register.register & 0xFF));

        return registerAddress;
    }

    public void setRegistersToSend(List<Register> registersToSend) {
        mRegistersToSend.clear();
        mRegistersToSend.addAll(registersToSend);
    }

    public void sendSameData(boolean sendSameData) {
        this.mSendSameData = sendSameData;
    }

    public abstract MessageComponents.MessagePart getBody();

    public MessageType getMessageTypeEnd() {
        return null;
    }

    public abstract MessageType getMessageType();
}
