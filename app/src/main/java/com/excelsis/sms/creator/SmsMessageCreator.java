package com.excelsis.sms.creator;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.excelsis.MainActivity;
import com.excelsis.Register;
import com.excelsis.Utils;
import com.excelsis.sms.MessageType;
import com.excelsis.sms.creator.bodyGenerators.BodyGenerator;
import com.excelsis.sms.decoder.typeEncoders.Type12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.excelsis.Utils.varToByteList;
import static com.excelsis.sms.creator.MessageComponents.*;


/**
 * Created by anze on 9.10.2016.
 */

public class SmsMessageCreator {
    private static final String DEBUG_TAG = "ComposeSmsMessage";

    private static final int FRAGMENT_LENGTH = 3;
    private static final int HEADER_LENGTH = 7;
    private static final int MAX_SMS_LENGTH = 130 - FRAGMENT_LENGTH;

    public String serialNumber = null;

    // if true than every message despite of length will be send
    private boolean mIgnoreMessageLength = true;
    private final Context context;

    private MessagePart registersBody;
    private MessageType messageType;
    private MessageType messageTypeEnd;

    public SmsMessageCreator(Context context) {
        this.context = context;
    }

    /**
     * Sends test message
     *
     * @param context to notify user that it is sending test message
     * @return generated test message
     */
    public byte[] getTestMessage(Context context) {
        // notify user that this is just a test message
        Toast.makeText(context, "Sending test message!", Toast.LENGTH_LONG).show();

        int[] data = {
                0x60, 0x09, 0x80, 0x63, 0x16,
                0x57, 0x18, 0x6A, 0x7B, 0x11, 0x00, 0x5B,
                0x00, 0x05, 0x01, 0x0C, 0x91, 0x44, 0x97, 0x42, 0x31, 0x65, 0x41, 0x01, 0x09,
                0x01, 0x00, 0x00, 0x30, 0x39, 0x01, 0x0B, 0x02, 0x0A, 0x00, 0x01, 0x0C, 0x03,
                0x00, 0x0A, 0x01, 0x0C, 0x04, 0x00, 0x98, 0x96, 0x7F, 0x01, 0x0C, 0x05, 0x1E,
                0x01, 0x0C, 0x06, 0x01, 0x01, 0x0C, 0x07, 0x00, 0x00, 0x07, 0x01, 0x10, 0x01,
                0x01, 0x00, 0x07, 0x03, 0x0A, 0x01, 0x00, 0x07, 0x04, 0x00, 0x01, 0x00, 0x07,
                0x05, 0x01, 0x00, 0x07, 0x10, 0x01, 0x0A, 0x01, 0x00, 0x07, 0x11, 0x01, 0x09,
                0x01, 0x00, 0x07, 0x12, 0x01, 0x09, 0x02, 0x00, 0x07, 0x13, 0x00, 0x00, 0x07
        };

        byte[] byteData = new byte[data.length];
        for (int i = 0; i < byteData.length; i++) {
            byteData[i] = (byte) data[i];
        }

        return byteData;
    }



    /**
     * Instead of getting one messages and then split it with heard get independent messages that
     * each have only one fragment and are not split up.
     */
    public Messages getIndependentMessages() {
        if(messageType == null) throw new IllegalStateException("Message type was not set!");

        // get all the registers that we will have to send
        ArrayList<MessagePart> registers = registersBody.messageParts;

        // all the message have this message type but the last one
        MessageType messageType = this.messageType;
        Messages messages = new Messages();
        int messageNumber = 123;
        for(int i=0; i<registers.size();) {
            MessagePart bodyData = new MessagePart("Body registers:");

            for(; i<registers.size(); i++) {
                MessagePart register = registers.get(i);
                if(bodyData.length() + register.length() < MAX_SMS_LENGTH - HEADER_LENGTH) {
                    // if we still have space left on the SMS message add the register
                    bodyData.addMessagePart(register);
                } else {
                    // this message is full. Stop creating it
                    break;
                }
            }

            // the last message type is different so that the device know that this is all of the
            // messages and that she can send us reply
            if(i == registers.size() && this.messageTypeEnd != null) messageType = this.messageTypeEnd;

            // generate message header
            MessagePart header = getDataHeader(bodyData.length(), messageType);

            // generate fragment header
            MessagePart fragment = getFragmentHeader(
                    bodyData.length() + header.length(),
                    0,
                    messageNumber,
                    true);
            messageNumber ++;

            // compose the whole message
            Message message = new Message("Message " + messages.size() + ":");
            message.addMessagePart(fragment);
            message.addMessagePart(header);
            message.addMessagePart(bodyData);
            messages.add(message);
        }

        return messages;
    }

    public void setMessageBodyGenerator(BodyGenerator generator) {
        registersBody = generator.getBody();
        messageType = generator.getMessageType();
        messageTypeEnd = generator.getMessageTypeEnd();
    }

    public Message getMessage(MessageType messageType){

        MessagePart bodyData = null;


        if(bodyData == null) {
            throw new NullPointerException("Unable to get message body data. Aborting.");
        }

        // generate message header
        MessagePart header = getDataHeader(bodyData.length(), messageType);

        // compose whole message
        Message message = new Message("Message");
        message.messageParts.add(header);
        message.messageParts.add(bodyData);

        return message;
    }


    public void ignoreMessageLength(boolean ignoreMessageLength) {
        this.mIgnoreMessageLength = ignoreMessageLength;
    }


    /*
    Get data hear for provided size of data
     */
    @SuppressWarnings("ConstantConditions")
    private MessagePart getDataHeader(int sizeOfData, MessageType msgType) {
        MessagePart header = new MessagePart("Header");

        // 1 byte message type 7:serial number is present, 6-0: message type,
        byte msg_type = (byte) msgType.getValue();
        boolean isSerialPresent = true;
        if (isSerialPresent) msg_type |= 0x80;
        header.addData(msg_type,
                String.format(Locale.getDefault(), "serial present: %s, %s(0x%02X)",
                        isSerialPresent + "",
                        msgType.toString(),
                        msgType.getValue()
                )
        );

        if (isSerialPresent) {
            if (serialNumber == null) {
                throw new NullPointerException("You forgot to set serialNumber variable!");
            }
            // 4 bytes serial number
            ArrayList<Byte> serialNumberBytes = getSerialNumberBytes(serialNumber);
            header.addData(serialNumberBytes, "serial number: '" + serialNumber + "'");
        }

        // 2 bytes for data length
        Byte[] dataLength = Utils.intToByteArray(sizeOfData, 2);
        header.addData(Arrays.asList(dataLength), "body size: " + sizeOfData);

        if (header.length() != HEADER_LENGTH) {
            throw new IllegalStateException("Data header is incorrect length!");
        }

        return header;
    }

    private ArrayList<Byte> getSerialNumberBytes(String serialNumber) {
        String whatsWrongWithSerNum = Utils.whatsWrongWithSerialNumber(serialNumber);
        if (whatsWrongWithSerNum != null) {
            throw new IllegalArgumentException(whatsWrongWithSerNum);
        }

        // 4 bytes, serial number of device, optional
        ArrayList<Byte> serialNumberData = new ArrayList<>();
        // append first character of serial number
        serialNumberData.add((byte) serialNumber.charAt(0));
        // convert the rest to int and append it as bytes
        int serialNumberInt = Integer.parseInt(serialNumber.substring(1));
        Byte[] serialNumberBytes = Utils.intToByteArray(serialNumberInt, 3);
        serialNumberData.addAll(Arrays.asList(serialNumberBytes));

        return serialNumberData;
    }

    /*
    Get data hear for provided data

    Message number: this number should be different for each message send for each configuration that is send
    FragmentNumber: If we need to divide message into multiple sms, each of them get's their own fragment number
     */
    private MessagePart getFragmentHeader(int sizeOfDataAndHeader, int fragmentNumber, int messageNumber, boolean isLast) {
        MessagePart fragment = new MessagePart("Fragment");

        // 2 bytes fragment number, 15:is last, 14-10:frag seq number, 9-0:msg seq number
        int fragMsgNum = messageNumber;
        fragMsgNum |= (fragmentNumber & 0x0F) << 10;
        if (isLast) fragMsgNum |= 0x8000; // add 1 to MSB if fragment is last
        fragment.addData(
                varToByteList(fragMsgNum, 2),
                String.format(Locale.getDefault(),
                        "Frag #:%d, Msg #:%d, isLast:%s",
                        fragmentNumber, messageNumber, isLast + ""
                )
        );
        // 1 byte for data length
        fragment.addData((byte) sizeOfDataAndHeader, "size: " + sizeOfDataAndHeader);

        if (fragment.length() > FRAGMENT_LENGTH) {
            throw new IllegalStateException("Fragment is longer then the max alloed length!");
        }

        return fragment;
    }


}
