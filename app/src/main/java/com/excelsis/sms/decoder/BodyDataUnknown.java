package com.excelsis.sms.decoder;



class BodyDataUnknown extends BodyData {
    public BodyDataUnknown(byte[] rawData, int start) {
        // just say that you parsed all of the data.
        bodyDataParsed = rawData.length - start;
        parsingResult = ParsingResult.SUCCESSFUL;
    }


}
