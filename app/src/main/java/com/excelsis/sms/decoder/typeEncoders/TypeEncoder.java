package com.excelsis.sms.decoder.typeEncoders;


import com.excelsis.Register;

/**
 * Created by anze on 8/23/17.
 */
public interface TypeEncoder {
    Byte[] encode(Register register);
}
