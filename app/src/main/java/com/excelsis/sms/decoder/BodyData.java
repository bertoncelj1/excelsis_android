package com.excelsis.sms.decoder;

import com.excelsis.sms.MessageType;

import java.util.Arrays;

public abstract class BodyData extends SmsMessageDecoder.Data {
    String parsingMessage;
    public String parsingMessageErr;

    enum ParsingResult {
        SUCCESSFUL,
        PARTIALY_SUCCESFUL,
        UNSCUCCESSFUL,
        UNKNOWN,
    }
    ParsingResult parsingResult = ParsingResult.UNKNOWN;


    int bodyDataParsed; // length of data that was parsed
    byte[] rawDataParsed; // raw data that was parsed

    /**
     * Parse the message body data. The body data can be different types depending on message type
     * And for each type different decoder needs to be called.
     *
     * Some decoders need register storage
     */
    public static BodyData parse(byte[] rawData, int start, MessageType messageType) {

        BodyData bodyData;
        switch(messageType) {
            case ACKNOWLEDGE:
                bodyData = new BodyDataAck(rawData, start);
                break;

            case REGISTER_READ_RESPONSE:
            case REGISTER_WRITE_ANDROID_END:
            case REGISTER_WRITE_ANDROID:
            case REGISTER_WRITE:
                bodyData = new BodyDataRegisters(rawData, start);
                break;

            default:
                bodyData = new BodyDataUnknown(rawData, start);
                break;
        }

        // save all the raw data that was parsed
        bodyData.rawDataParsed = Arrays.copyOfRange(rawData, start, start + bodyData.bodyDataParsed);
        bodyData.rawData = Arrays.copyOfRange(rawData, start, rawData.length);

        return bodyData;
    }

        /*
        TODO It would be nice if the length of body data is checked before parsing.
        @SuppressLint("DefaultLocale")
        protected boolean checkLength(byte[] rawData, int start, int expectedLength) {
            int dataLen = rawData.length - start;
            if(dataLen < expectedLength) {
                parsingMessageErr = String.format("Expected body data too short. Expected:%d, Got:%d", expectedLength, dataLen);
                parsingMessageErr = "\nUnable to parse this body!";
                parsingResult = ParsingResult.UNSCUCCESSFUL;
            } if(bodyDataLen > expectedLength) {
                parsingMessageErr = String.format("Expected body data too short. Expected:%d, Got:%d", expectedLength, dataLen);
                parsingMessageErr = "\nParsing only the first value ignoring the rest";
                parsingResult = ParsingResult.PARTIALY_SUCCESFUL;
            } else {
                parsingResult = ParsingResult.SUCCESSFUL;
            }
        }
        */
}
