package com.excelsis.sms.decoder;

import android.content.Context;
import android.util.Log;

import com.excelsis.Register;
import com.excelsis.Utils;
import com.excelsis.sms.MessageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.excelsis.Utils.splitInt;
import static com.excelsis.sms.decoder.DecoderHelpers.byteArrayToInt;


/**
 * Created by anze on 10/23/16.
 */
public class SmsMessageDecoder {

    private static final String DEBUG_TAG = "SmsMessageDecoder";


    public SmsMessageDecoder(Context context){

    }


    public static class Data {
        byte[] rawData; // raw data with register and channel included

        // returns value in user readable format
        String getRawDataStr(){
            return Utils.arrayToString(rawData);
        }

        @Override
        public String toString() {
            return this.getRawDataStr();
        }
    }

    public static class RegisterData extends Data {
        String name;
        public String value;

        public Register register;

        @Override
        public String toString() {

            // if value is not available show raw data
            if(value == null) {
                return super.getRawDataStr();
            }

            // show EMPTY_VALUE_STRING when value is empty
            return value;
        }

    }


    public static class FragmentHeaderData extends Data {
        public int messageSeqNum;
        public int fragmentSeqNumber;
        public boolean isLast;
        public int length;

        private FragmentHeaderData(byte[] data, int start) {
            int absoluteStart = start;

            // 2 bytes of fragment id
            int fragmentId = byteArrayToInt(data, start, 2);
            start += 2;

            // 0-9 bits: Message sequence number: Increments with each message.
            // All fragments of the same message hate the same sequence number
            messageSeqNum = splitInt(0, 9, fragmentId);

            // 10-14 bits: Fragment sequence number: Increments with each fragment.
            // Each message can have a maximum of 20 fragments. The first fragment of a message
            // always has sequence number zero
            fragmentSeqNumber = splitInt(10, 14, fragmentId);

            // 15 bit: 0 more fragments, 1-final fragment of the message.
            isLast = splitInt(15, 15, fragmentId) == 1;

            // 1 byte fragment sequence. 80 means only one
            length = byteArrayToInt(data, start, 1);
            start += 1;

            rawData = Arrays.copyOfRange(data, absoluteStart, start);
        }

        public static FragmentHeaderData parse(byte[] data, int start){
            return new FragmentHeaderData(data, start);
        }
    }


    public static class MessageHeaderData extends Data {
        public int messageTypeInt;
        public MessageType messageType;
        boolean isSerialPresent;
        public int serialNumber;
        public int dataLength;

        private MessageHeaderData(byte[] data, int start){
            int absoluteStart = start;

            // 1 byte message flag
            int rawMsgType = byteArrayToInt(data, start, 1);
            start += 1;

            // 0-6: Message type
            // 0Defines the identification code of message type
            messageTypeInt = splitInt(0, 6, rawMsgType);
            messageType = MessageType.fromInt(messageTypeInt);

            // 7: Defines if serial number (SERIAL_NR) is present
            isSerialPresent = splitInt(0, 6, rawMsgType) != 0;

            if(isSerialPresent) {
                // 4 bytes of serial number
                serialNumber = byteArrayToInt(data, start, 4);
                start += 4;
            }

            // 2 bytes length of the data
            dataLength = byteArrayToInt(data, start, 2);
            start += 2;

            super.rawData = Arrays.copyOfRange(data, absoluteStart, start);
        }

        static MessageHeaderData parse(byte[] data, int start) {
            return new MessageHeaderData(data, start);
        }
    }


    public class Message extends Data {
        public FragmentHeaderData fragmentHeaderData;
        public MessageHeaderData messageHeaderData;
        public BodyData bodyData;
        List<Data> datas = new ArrayList<>();

        public MessageType messageType;

        Message(byte[] rawData) {
            super.rawData = rawData;
            int start = 0;

            // decode fragment header
            fragmentHeaderData = FragmentHeaderData.parse(rawData, start);
            start += fragmentHeaderData.rawData.length;

            // decode data header
            messageHeaderData = MessageHeaderData.parse(rawData, start);
            start += messageHeaderData.rawData.length;
            messageType = messageHeaderData.messageType;

            // check if the data header length matches the actual length
            if(start + messageHeaderData.dataLength != rawData.length) {
                Log.e(DEBUG_TAG, "Wrong data length: " + messageHeaderData.dataLength);
            }

            // parse body data
            bodyData = BodyData.parse(rawData, start, messageType);
            logParsingResult(bodyData.parsingResult);
        }
    }


    public void logParsingResult(BodyData.ParsingResult parsingResult) {
        switch(parsingResult){
            case SUCCESSFUL:
                Log.d(DEBUG_TAG, "Body data parsing successful");
            case PARTIALY_SUCCESFUL:
                Log.w(DEBUG_TAG, "Body data parsing only partially successful");
            case UNSCUCCESSFUL:
                Log.e(DEBUG_TAG, "Unable to parse body data!");
            case UNKNOWN:
                Log.d(DEBUG_TAG, "Parser didn't report if the parsing of body data was successful");
        }
    }

    public Message decodeMessage(byte[] rawMessage) {
        return new Message(rawMessage);
    }


}
