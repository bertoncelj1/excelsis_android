package com.excelsis.sms.decoder.typeEncoders;


import com.excelsis.Register;
import com.excelsis.sms.decoder.SmsMessageDecoder;

/**
 * Created by anze on 8/23/17.
 *
 * Decode phone number
 */
public class Type12 implements TypeEncoder {

    // international and local constants put in front of
    // the phone number. They are defined by standard (which?)
    public static byte INTERNATIONAL = (byte) 0x91;
    public static byte LOCAL = (byte) 0x81;


    private boolean international = false;

    /**
     * Check if number is international or not
     * @param pNumber phone number
     * @return phone number without "00" or "+" prefix
     */
    private String encodeStart(String pNumber){
        if(pNumber.startsWith("00")){
            international = true;
            return pNumber.substring(2);
        }else if(pNumber.startsWith("+")){
            international = true;
            return pNumber.substring(1);
        }else {
            international = false;
            return pNumber;
        }
    }

    @Override
    public Byte[] encode(Register register) {
        String pNumber = encodeStart(register.value);

        int len;
        len = pNumber.length();

        int byteLen = (len % 2 == 1? len+1 : len)/2; // if the number is odd byteLen is (len+1)/2
        if(byteLen > 11) throw new IllegalArgumentException(
                "Phone number byte length longer than 11. Byte len:" + byteLen + " Phone number length: " + len);

        Byte[] b = new Byte[byteLen+2];

        int j = 0;
        b[j++] = (byte) len;
        b[j++] = international ? INTERNATIONAL : LOCAL;

        for(int i=0; i<len; i+=2, j++) {
            // convert numbers to values
            int first = pNumber.charAt(i) - '0';
            int second = i+1 < len ? pNumber.charAt(i+1) - '0' : 0xF; // add 0xF to the end if the number is odd

            // switch numbers around and write them both into single byte
            b[j] = (byte) ((second << 4) + first);
        }

        return b;
    }

     /**
     * Phone number format
      *
      * Just copy the raw value and call it a day
     * @param messageData where to save vale
     * @param data raw data to read from
     * @param start where the register is located in data
     * @return length of the register
     */
    public static int decode(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        int len = 0;

        messageData.value = "";
        do {
            len ++;
            messageData.value += String.format("%02X", data[start]);
        } while((data[start++] & 0xF0) != 0xF0 || start >= data.length);

        return len;
    }

}
