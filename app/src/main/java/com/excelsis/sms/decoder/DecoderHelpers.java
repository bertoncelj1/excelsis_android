package com.excelsis.sms.decoder;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class DecoderHelpers {


    public static int byteArrayToInt(byte[] data, int start, int len){
        if(len > 4) throw new IllegalArgumentException("int is to short for length > of 4 bytes!");
        if(data.length < start+len) throw new IllegalArgumentException(
                "Data to short for parameters: start:" + start + " len:" + len + ". Data len: " + data.length);

        int value = 0;

        // TODO check if big or little indian is required
        for(int i=start; i < start + len; i++){
            value = value << 8;
            value += (int) (data[i] & 0xFF);
        }

        return value;
    }


    public static String byteArrayToString(byte[] data, int start, int len){
        if(len == 0) return "";

        byte[] d = Arrays.copyOfRange(data, start, start + len);
        String value = new String(d);
        return value;
    }

    /**
     * Insert char in specific postion in string
     * @param x string in witch char will be inserted
     * @param position position of inserted char
     * @param c char to be inserted
     * @return .
     */
    static String insertCharacter(String x, int position, char c){
        if(position > x.length()) return x;

        return x.substring(0, position) + c + x.substring(position, x.length());
    }
}
