package com.excelsis.sms.decoder;

import static com.excelsis.sms.decoder.DecoderHelpers.byteArrayToInt;

public class BodyDataAck extends BodyData {
    public int ackOKint;
    public boolean ackOK;

    BodyDataAck(byte[] rawData, int start) {
        // 1 for OK, 0 or any other number for Error
        ackOKint = byteArrayToInt(rawData, start, 1);
        ackOK = ackOKint == 1;
        bodyDataParsed = 1;
        parsingResult = ParsingResult.SUCCESSFUL;
    }

}
