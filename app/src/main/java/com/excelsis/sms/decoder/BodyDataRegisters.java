package com.excelsis.sms.decoder;


import android.util.Log;

import com.excelsis.Register;
import com.excelsis.RegisterLibrary;
import com.excelsis.RegisterLoader;
import com.excelsis.sms.decoder.typeEncoders.Type12;
import com.excelsis.sms.decoder.typeEncoders.Type123;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import au.com.bytecode.opencsv.CSVReader;

import static com.excelsis.sms.decoder.DecoderHelpers.byteArrayToInt;
import static com.excelsis.sms.decoder.DecoderHelpers.byteArrayToString;
import static com.excelsis.sms.decoder.DecoderHelpers.insertCharacter;

;


public class BodyDataRegisters extends BodyData {
    private static final String DEBUG_TAG = "BodyDataRegisters";

    public ArrayList<SmsMessageDecoder.RegisterData> registers = new ArrayList<>();
    RegisterLibrary decodeRegisters = RegisterLibrary.getDecodeRegisters();

    public BodyDataRegisters(byte[] rawData, int start) {
        try {
            // encode registers stored in data
            while(start < rawData.length) {
                SmsMessageDecoder.RegisterData registerData = decodeRegister(rawData, start);

                registers.add(registerData);
                start += registerData.rawData.length;
            }

        } catch (Exception e) {
            super.parsingMessageErr = "Unable to encode all message data: " + e.getMessage();
            Log.e(DEBUG_TAG, e.getMessage(), e);
            e.printStackTrace();
            Log.e(DEBUG_TAG, "Unable to encode all message data.");
        }
    }
        /*
    Decode register and returns Data containing register value, name, rawData ...
    register form <channel 1 byte><register 2 bytes><data length is depended on register type>
    */
    private SmsMessageDecoder.RegisterData decodeRegister(byte[] data, int start) throws RegisterDecodingException {
        final int absoluteStart = start; // start variable changes but this one stays the same

        // first byte is channel
        int channel = byteArrayToInt(data, start, 1);
        start += 1;

        // second and third bytes are register number
        int registerNumber = byteArrayToInt(data, start, 2);
        start += 2;

        // gets registerKey and than register
        int registerKey = Register.getKey(channel, registerNumber);
        Register register = decodeRegisters.getRegisters().get(registerKey + "");

        if(register == null){
            Log.e(DEBUG_TAG, "Cannot find register with key: "  + Register.keyToString(registerKey));
            throw new RegisterDoesNotExists(registerKey);
        }

        // creates message data
        SmsMessageDecoder.RegisterData messageData = new SmsMessageDecoder.RegisterData();
        messageData.name = register.preferenceKey;
        messageData.register = register;

        // decodes register depending on register type
        // 41f 16, 520 1, 101 6, 507 2, 508 2, 209 2, 207 2, 202 19
        int valueLength;
        switch(register.dataType){

            // modbus registers, in type 123. Modbus register type is written
            case 123:
                valueLength = (new Type123(decodeRegisters)).decode(messageData, data, start);
                break;
            // modbus register. In this register modbus value is written, It is important that
            // first we get the type of the modbus value and then -123. Basically the type of -123
            // should be handled inside the Type123 decoder.
            case -123:
                throw new ModbusRegisterException(
                        "Register with value (tyoe -123) must be " +
                                "preceded by register with type 123.");

            case 1:
                valueLength = decodeValueType1(messageData, data, start);
                break;

            case 2:
                valueLength = decodeValueType2(messageData, data, start);
                break;

            case 4:
                valueLength = decodeValueType4(messageData, data, start);
                break;

            case 6:
                valueLength = decodeValueType6(messageData, data, start);
                break;

            case 12:
                valueLength = Type12.decode(messageData, data, start);
                break;

            case 13:
                valueLength = decodeValueType13(messageData, data, start);
                break;

            case 44:
                valueLength = decodeValueType44(messageData, data, start);
                break;

            case 16:
            case 19:
            case 128:
                valueLength = decodeValueTypeStr(messageData, data, start);
                break;

            default:
                throw new RegisterDecodingException("Don't know how to encode register with that type. Register: " + register.toString());
        }


        modifyValue(messageData, register);

        start += valueLength;
        messageData.rawData = Arrays.copyOfRange(data, absoluteStart, start);

        return messageData;
    }

    /**
     * Modifies value if necessary. Like inserst decimal point  ....
     * @param messageData contains value that needs to be modefied
     * @param register how value needs to be modefied
     */
    private void modifyValue(SmsMessageDecoder.RegisterData messageData, Register register){
        if(messageData.value == null) return;

        String x = messageData.value;
        switch(register.register){
            case 0x0209: // voltage
                messageData.value = insertCharacter(messageData.value, 1, '.') + " V";
                break;

            case 0x0207: // temperature
                messageData.value = insertCharacter(messageData.value, 2, '.') + " °C";
                break;

        }

    }


    private int decodeValueType1(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        int value = byteArrayToInt(data, start, 1);

        messageData.value = value + "";
        return 1;
    }

    private int decodeValueType2(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        int value = byteArrayToInt(data, start, 2);

        messageData.value = value + "";
        return 2;
    }

    private int decodeValueType4(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        int value = byteArrayToInt(data, start, 4);

        messageData.value = value + "";
        return 4;
    }

    private int decodeValueType6(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        // 6 bytov leto mesec dan ura minuta sekunda
        int year = data[start];
        int month = data[start + 1];
        int day = data[start + 2];
        int hour = data[start + 3];
        int minutes = data[start + 4];
        int seconds = data[start + 5];

        messageData.value = String.format(Locale.getDefault(),
                "%02d/%02d/%02d %02d:%02d:%02d", year, month, day, hour, minutes, seconds);
        return 6;
    }


    /**
     * Format: 3 Chars each indicating it's own status
     * these three chars represent RTC_SYCH, SMS, GPRS
     * their values can be 0:NONE, 1:OK, 3:ERROR
     * @param messageData where to save vale
     * @param data raw data to read from
     * @param start wher the register is located in data
     * @return length of the register
     */
    private int decodeValueType13(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        int length = byteArrayToInt(data, start, 1);

        String message = byteArrayToString(data, start + 1, length);

        if (length == 3) {
            // read chars and conver them to readable status
            String rtc = statusToStringType13(message.charAt(0));
            String sms = statusToStringType13(message.charAt(1));
            String gprs = statusToStringType13(message.charAt(2));
            messageData.value = String.format("RTC_SYCH:%s SMS:%s GPRS:%s", rtc, sms, gprs);
        }else{
            messageData.value = "no data";
        }
        return length + 1;
    }

    private int decodeValueType44(SmsMessageDecoder.RegisterData messageData, byte[] data, int start){
        int value = byteArrayToInt(data, start, 4);
        float floatValue = Float.intBitsToFloat(value);
        messageData.value = String.format(Locale.getDefault(), "%.2f", floatValue);
        return 4;
    }

    /**
     * Decodes type13 message status chars to string
     * 0:NONE, 1:OK, 3:ERROR
     * @return string represented status value
     */
    private String statusToStringType13(char c){
        switch(c){
            case '0': return "None";
            case '1': return "OK";
            case '3': return "Error";
            default: return "Unknown '" + c + "'";
        }
    }

    /**
     * Format: String
     * First byte is length others are value usually chars
     * @param messageData where to save vale
     * @param data raw data to read from
     * @param start wher the register is located in data
     * @return length of the register
     */
    private int decodeValueTypeStr(SmsMessageDecoder.RegisterData messageData, byte[] data, int start) throws RegisterDecodingException {
        int length = byteArrayToInt(data, start, 1);

        /*
        if (data.length < start+length) {
            throw new RegisterDecodingException(
                    "Cannot read string value. String length ("+length+") exceeds data.length("+data.length+")");
        }
        */
        messageData.value = "\"" + byteArrayToString(data, start + 1, length) + "\"";
        return length + 1;
    }

    public static class RegisterDecodingException extends Exception {
        public RegisterDecodingException(String message) {
            super(message);
        }
    }

    public static class ModbusRegisterException extends RegisterDecodingException {
        public ModbusRegisterException(String message) {
            super(message);
        }
    }

    public static class RegisterDoesNotExists extends RegisterDecodingException {
        public RegisterDoesNotExists(int registerKey) {
            super("Cannot find register with key: "  + Register.keyToString(registerKey));
        }
    }
}
