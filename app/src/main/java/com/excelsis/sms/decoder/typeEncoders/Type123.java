package com.excelsis.sms.decoder.typeEncoders;


import android.util.Log;

import com.excelsis.Register;
import com.excelsis.RegisterLibrary;
import com.excelsis.sms.decoder.BodyDataRegisters;
import com.excelsis.sms.decoder.SmsMessageDecoder;

import java.nio.ByteBuffer;
import java.util.Locale;

import static com.excelsis.sms.decoder.DecoderHelpers.byteArrayToInt;

/**
 * Created by bertoncelj on 8/23/17.
 *
 * Decode modbus registers
 *
 * Modbus registers are written inside two registers
 * The first one just gives you the modbus value type of the register and has our type of 123
 * and the second has to have Our type of -123 and gives you the actual value.
 */
public class Type123 implements TypeEncoder {

    private static final String DEBUG_TAG = "Modbus type 123";
    RegisterLibrary decodeRegisters;

    public Type123(RegisterLibrary decodeRegisters) {
        this.decodeRegisters = decodeRegisters;
    }

    @Override
    public Byte[] encode(Register register) {
        // we don't need to encode for now
        throw new NullPointerException();
    }

     /**
     * Decodes modbus register
     *
     * @param messageData where to save vale
     * @param data raw data to read from
     * @param start where the register is located in the data
     * @return number of bytes read
     */
     public int decode(SmsMessageDecoder.RegisterData messageData, byte[] data, int start) throws BodyDataRegisters.RegisterDecodingException {
         int startStart = start; // save where the start was at the start of these function so we can return how many bytes have we read
         int modbusRegisterType = byteArrayToInt(data, start, 1);
         start += 1;

         // first byte is channel
         int channel = byteArrayToInt(data, start, 1);
         start += 1;

         // second and third bytes are register number
         int registerNumber = byteArrayToInt(data, start, 2);
         start += 2;

         // gets registerKey and than register
         int registerKey = Register.getKey(channel, registerNumber);
         Register register = decodeRegisters.getRegisters().get(registerKey + "");

         // change messageData register because from register we also read register name (description)
         // and we want to get that from the second register and not the first one
         messageData.register = register;

         if (register == null) {
             Log.e(DEBUG_TAG, "Cannot find register with key: " + Register.keyToString(registerKey));
             throw new BodyDataRegisters.RegisterDoesNotExists(registerKey);
         }

         // the second register Needs to have type -123
         if (register.dataType != -123) {
             throw new BodyDataRegisters.ModbusRegisterException(
                     "Expected modbus register with type: -123. Got: " + register.dataType);
         }

         ByteBuffer bb = ByteBuffer.wrap(data, start, data.length - start);
         switch (modbusRegisterType) {
             case 0x00: // unsigned 8 bit - unknown type is 1 byte long
             case 0x01: // unsigned 8 bit
                 messageData.value = (short) (bb.get() & (short) 0xff) + "";
                 start += 1;
                 break;

             case 0x11: // signed 8 bit
                 messageData.value = bb.get() + "";
                 start += 1;
                 break;

             case 0x02: // unsigned 16 bit
                 messageData.value = (int) (bb.getShort() & 0xffff) + "";
                 start += 2;
                 break;

             case 0x12: // signed 16 bit
                 messageData.value = bb.getShort() + "";
                 start += 2;
                 break;

             case 0x04: // unsigned 32 bit
                 messageData.value = ((long) bb.getInt() & 0xffffffffL) + "";
                 start += 4;
                 break;

             case 0x14: // signed 32 bit
                 messageData.value = bb.getInt() + "";
                 start += 4;
                 break;

             case 0x08: // unsigned 64 bit
                 // we are not parsing it as a unsigned long but as a signed long
                 // we assume that values will be smaller then 2^31
                 messageData.value = bb.getLong() + "";
                 start += 8;
                 break;

             case 0x18: // signed 64 bit
                 messageData.value = bb.getLong() + "";
                 start += 8;
                 break;

             case 0x44: // float
                 messageData.value = String.format(Locale.getDefault(), "%.3f", bb.getFloat());
                 start += 4;
                 break;

             case 0x88: // double
                 messageData.value = String.format(Locale.getDefault(), "%.6f", bb.getDouble());
                 start += 4;
                 break;

             case 0xF8: // special total format
                 // u32int + u32int / 10^9
                 long part1 = ((long) bb.getInt() & 0xffffffffL);
                 long part2 = ((long) bb.getInt(4) & 0xffffffffL);
                 messageData.value = String.format(
                         Locale.getDefault(),
                         "%.6f",
                         part1 + part2 / (double) 1e9
                 );
                 start += 8;
                 break;

             case 0xFF: // string format
                 StringBuilder a = new StringBuilder();
                 for(; data[start] != 0; start++) {
                       a.append(data[start]);
                 }
                 messageData.value = a.toString();

             default:
                 throw new BodyDataRegisters.RegisterDecodingException("Don't know how to decode register with that modbus type. Type: " + modbusRegisterType);
         }

         return start - startStart;
     }
}
