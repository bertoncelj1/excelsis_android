package com.excelsis.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.excelsis.Utils;
import com.excelsis.displayer.ActivitySmsDisplayer;
import com.excelsis.sms.decoder.SmsMessageDecoder.FragmentHeaderData;
import com.excelsis.sms.recievedDatabase.DatabaseClient;
import com.excelsis.sms.recievedDatabase.Message;
import com.excelsis.sms.recievedDatabase.MessageDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.excelsis.Utils.addBytesToList;
import static com.excelsis.sms.recievedDatabase.MessageDao.Helper.oldestAllowedMessage;


public class SmsReceiver extends BroadcastReceiver {

	private static final String DEBUG_TAG = "SmsReceiver";

	public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(DEBUG_TAG, "Message received" + intent.getDataString());
		byte[] binaryMessage;

		// get pdu from bundle
		Bundle bundle = intent.getExtras();
		if(bundle == null) {
			Log.e(DEBUG_TAG, "Unable to get message bundle");
			return;
		}
		Object[] pdus = (Object[]) bundle.get("pdus");
		if(pdus == null){
			Log.e(DEBUG_TAG, "Unable to get pdus from bundle");
			return;
		}

		SmsMessage[] msgs = new SmsMessage[pdus.length];

		if(intent.getAction().equals(SMS_RECEIVED_ACTION)){
			// read text message
			Log.i(DEBUG_TAG, "The message is not binary. This message is not for me.");
		} else {
			// read binary messages
			ArrayList<Message> messages = new ArrayList<>(1);
			for (int i=0; i<msgs.length; i++){
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				binaryMessage = msgs[i].getUserData();
				String telNumber = msgs[i].getOriginatingAddress();
				long timestamp = msgs[i].getTimestampMillis();
				int status = msgs[i].getStatus();

				// create new message that will be saved into the DB
				Message msg = new Message();
				msg.senderTelNumber = telNumber;
				msg.rawMessage = binaryMessage;
				msg.pduTimestamp = timestamp;
				// parse/decode fragment and save data into message
				FragmentHeaderData fragment =
						FragmentHeaderData.parse(binaryMessage, 0);
				msg.fragmentNumber = fragment.fragmentSeqNumber;
				msg.messageNumber = fragment.messageSeqNum;
				msg.isLast = fragment.isLast;

				messages.add(msg);
			}

			Log.d(DEBUG_TAG, "Got " + messages.size() + " message(s): ");
			for(Message msg: messages) {
				Log.d(DEBUG_TAG, msg.toLongString());
			}
			Log.v(DEBUG_TAG, "Saving all messages into database");

			// save messages to db
			MessageDao md = DatabaseClient.getDatabase(context).messageDao();
			new Thread(() -> {
				md.insertAll(messages);
				findAndShowCompleteMessages(context);
			}).start();
		}
	}

	public static void showMessage(Context context, String from, byte[] binaryMessage) {
		Intent showMessage = new Intent(context, ActivitySmsDisplayer.class);
		showMessage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		showMessage.putExtra("from", from);
		showMessage.putExtra("binaryMessage", binaryMessage);
		context.startActivity(showMessage);
	}

	public static void findAndShowCompleteMessages(Context context) {
		MessageDao md = DatabaseClient.getDatabase(context).messageDao();
		List<Message> allMessages = MessageDao.Helper.getAllNotCompleted(md);
		Log.d(DEBUG_TAG, "Trying to find complete messages. Num msgs: " + allMessages.size());
		for(Message msg: allMessages) {
			Log.v(DEBUG_TAG, msg.toLongString());
		}

		for(Message message : allMessages) {
			if(message.state != Message.STATE_NOT_PROCESSED) {
				Log.v(DEBUG_TAG, "Message " + message.id + " already processed. Skipping");
				continue;
			}

			// find all pair messages of this message and try to combine them together
			List<Message> pairMessages = findAllPairMessages(allMessages, message);

			Message lastMessage = pairMessages.get(pairMessages.size() - 1);
			if(lastMessage.isLast) {
				if(lastMessage.fragmentNumber == pairMessages.size() - 1) {
					if(lastMessage.fragmentNumber == 0) {
						Log.d(DEBUG_TAG, "This message "+ lastMessage.id + " is single. Just showing it");
						// single fragment message ... just show it
						Log.v(DEBUG_TAG, "Showing message " + message.id);
						showMessage(context, lastMessage.senderTelNumber, lastMessage.rawMessage);
						// mark message as processed so it is not analysed again
						setMessageState(allMessages, lastMessage, Message.STATE_PROCESSED);
					} else {
						Log.d(DEBUG_TAG, "Multi part message. Combining them together.");
						// multi part fragment message
						// combine all fragment messages into one
						ArrayList<Byte> fullMessage = new ArrayList<>();
						String combineMessageIds = "";
						int expectedFragmentNumber = 0;
						for(Message pairMessage : pairMessages) {
							if(expectedFragmentNumber != pairMessage.fragmentNumber) {
								pairMessage.state = Message.STATE_PROCESSED;
								Log.w(DEBUG_TAG, String.format(
										"Incorrect fragment number expected %d got %d instead. Ignoring also all paired messages: %s",
										expectedFragmentNumber,
										pairMessage.fragmentNumber,
										pairMessage.toLongString())
								);
							} else {
								int startAt = pairMessage.fragmentNumber == 0? 0 : 3;
								addBytesToList(fullMessage, pairMessage.rawMessage, startAt);
								combineMessageIds += pairMessage.id + " ";
							}
							// mark message as complete so it is not analysed again
							setMessageState(allMessages, pairMessage, Message.STATE_PROCESSED);
							expectedFragmentNumber++;
						}

						if(expectedFragmentNumber == pairMessages.size()) {
							Log.v(DEBUG_TAG, "Showing multipart message. Ids: " + getMessagesIds(pairMessages));

							byte[] binaryMessage = Utils.toByteArray(fullMessage);
							Message msg = new Message();
							msg.senderTelNumber = lastMessage.senderTelNumber;
							msg.rawMessage = binaryMessage;
							msg.pduTimestamp = -1;
							msg.fragmentNumber = -1;
							msg.messageNumber = -1;
							msg.combineMessageIds = combineMessageIds;
							msg.isLast = true;
							msg.state = Message.STATE_COMBINED;
							md.insertAll(Collections.singletonList(msg));

							// only show message if all fragments have been added to the message
							showMessage(context, lastMessage.senderTelNumber, binaryMessage);
						}
					}

				} else if(lastMessage.fragmentNumber < pairMessages.size() - 1) {
					Log.d(DEBUG_TAG, "Too many messages for this message number. Trying to find single messages.");
					// if there are more messages then there is fragment numbers that means that
					// some messages have duplicated messageNumber. We go trough message
					// and try to salvage some of the the messages that are singles (meaning they
					// are composed of only one fragment)
					for(Message pairMessage : pairMessages) {
						if(pairMessage.isLast && pairMessage.fragmentNumber == 0) {
							Log.v(DEBUG_TAG, "Showing salvaged message: " + pairMessage.id);
							showMessage(context, lastMessage.senderTelNumber, pairMessage.rawMessage);
						} else {
							Log.w(DEBUG_TAG, "Unable to find pair for this message. Marking it as processed. Message: " + pairMessage.toLongString());
						}

						// even if it has not been show we mark it as complete, this we kinda
						// discard this message so it is not messing up any new messages with same
						// message number. We only do this when isLast > numMessages which assures
						// that there are too many messages with this fragment id and that we can discard them
						// mark message as processed so it is not analysed again
						setMessageState(allMessages, pairMessage, Message.STATE_IGNORED);
					}
				}
			} else {
				Log.v(DEBUG_TAG, "Message " + message.id + " is not last. Nothing to do");
			}
		}

		md.updateAll(allMessages);
	}

	public static List<Message> findAllPairMessages(List<Message> allMessages, Message pair) {
		ArrayList<Message> pairMessages = new ArrayList<>();
		long oldestAllowedMessage = oldestAllowedMessage();

		for(Message message : allMessages) {
			if(pair.state != Message.STATE_NOT_PROCESSED) continue;
			if(pair.messageNumber != message.messageNumber) continue;
			if(!pair.senderTelNumber.equals(message.senderTelNumber)) continue;
			if(pair.pduTimestamp < oldestAllowedMessage) continue;

			pairMessages.add(message);
		}

		// sort by fragment number increasing
		Collections.sort(pairMessages, (m1, m2) -> m1.fragmentNumber - m2.fragmentNumber);

		return pairMessages;
	}

	private static String getMessagesIds(List<Message> messages) {
		StringBuilder out = new StringBuilder();
		for (Message message : messages) {
			out.append(message.id).append(" ");
		}
		return out.toString();
	}

	private static void setMessageState(List<Message> allMessages, Message pairMessage, int state) {
		Log.v(DEBUG_TAG, "Setting message " + pairMessage.id + " as " + Message.getStateStr(state));
		Message msg = getMessageById(allMessages, pairMessage.id);
		msg.state = state;
	}

	public static Message getMessageById(List<Message> messages, int messageId) {
		for(Message message : messages) {
			if(message.id == messageId) return message;
		}
		return null;
	}
}