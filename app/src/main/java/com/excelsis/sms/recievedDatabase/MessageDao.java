package com.excelsis.sms.recievedDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Dao
public interface MessageDao {
    @Query("SELECT * FROM message ORDER BY createdInDbTimestamp DESC")
    List<Message> getAll();

    @Query("SELECT * FROM message " +
            "WHERE state = "  + Message.STATE_NOT_PROCESSED + " " +
            "AND pduTimestamp > :oldestAllowed ")
    List<Message> getAllNotProcessed(long oldestAllowed);

    @Query("SELECT * FROM message ORDER BY createdInDbTimestamp DESC")
    LiveData<List<Message>> getAllAsync();

    @Query("SELECT * FROM message WHERE id IN (:messageIds)")
    List<Message> loadAllByIds(int[] messageIds);

    @Update
    int updateAll(List<Message> messages);

    @Insert
    void insertAll(List<Message> messages);

    @Insert
    void insertAllArgs(Message... messages);

    @Delete
    void delete(Message message);

    class Helper {
        // messages older then oldest allowed are too old to be processed and
        // are thus ignored
        public static long oldestAllowedMessage() {
            long now = System.currentTimeMillis();
            return now - TimeUnit.DAYS.toMillis(2);
        }

        public static List<Message> getAllNotCompleted(MessageDao dao) {
            return dao.getAllNotProcessed(oldestAllowedMessage());
        }
    }
}


