package com.excelsis.sms.recievedDatabase;

import android.content.Context;

import androidx.room.Room;

public class DatabaseClient {

    private Context mCtx;
    private static DatabaseClient mInstance;

    //our app database object
    public static final String DB_NAME = "sms_database_9";
    private MessagesDatabase database;

    private DatabaseClient(Context mCtx) {
        this.mCtx = mCtx;
        // create the database
        database = Room.databaseBuilder(mCtx, MessagesDatabase.class, DB_NAME).build();
    }

    public static synchronized MessagesDatabase getDatabase(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance.database;
    }
}