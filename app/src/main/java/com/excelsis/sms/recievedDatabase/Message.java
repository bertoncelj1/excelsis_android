package com.excelsis.sms.recievedDatabase;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.excelsis.Utils.arrayToString;

@Entity
public class Message implements Serializable {

    public static final int STATE_NOT_PROCESSED = 0;
    public static final int STATE_PROCESSED = 1;
    public static final int STATE_IGNORED = 2;
    public static final int STATE_COMBINED = 4;

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "senderTelNumber")
    public String senderTelNumber;

    @ColumnInfo(name = "pduTimestamp")
    public long pduTimestamp;

    @ColumnInfo(name = "messageNumber")
    public int messageNumber = -1;

    @ColumnInfo(name = "fragmentNumber")
    public int fragmentNumber = -1;

    @ColumnInfo(name = "rawMessage")
    public byte[] rawMessage = new byte[0];

    @ColumnInfo(name = "createdInDbTimestamp")
    public long createdInDbTimestamp = System.currentTimeMillis();

    @ColumnInfo(name = "isLast")
    public boolean isLast = false;

    @ColumnInfo(name = "combineMessageIds")
    public String combineMessageIds = null;

    // if all fragments of this message have arrived
    // it also means that this message was processed and it is ignored when
    // trying to process other messages
    @ColumnInfo(name = "state")
    public int state = STATE_NOT_PROCESSED;

    public static String getStateStr(int messageState) {
        switch (messageState) {
            case Message.STATE_IGNORED:
                return "ignored";

            case Message.STATE_PROCESSED:
                return "processed";

            case Message.STATE_NOT_PROCESSED:
                return "not processed";

            case Message.STATE_COMBINED:
                return "combined";

            default:
                return "Unknown message state: " + messageState;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.getDefault(),
                "%3d age:%.2f tel:%s dataLen:%d",
                id,
                TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis() - pduTimestamp) / 24f,
                senderTelNumber,
                rawMessage.length);
    }

    @NonNull
    public String toLongString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.GERMANY);

        if (state == STATE_COMBINED)
            return String.format(Locale.getDefault(),
                    "%-3d tel: %s\ntime:%s\nids:%s state:%s\ndata:%s",
                    id,
                    senderTelNumber,
                    formatter.format(new Date(createdInDbTimestamp)),
                    combineMessageIds,
                    getStateStr(state),
                    arrayToString(rawMessage));
        else
            return String.format(Locale.getDefault(),
                    "%-3d tel: %s\npdu:%s\ndb:%s\n(msg#:%d frag#:%3d isLast:%s) state:%s\ndata:%s",
                    id,
                    senderTelNumber,
                    formatter.format(new Date(pduTimestamp)),
                    formatter.format(new Date(createdInDbTimestamp)),
                    messageNumber,
                    fragmentNumber,
                    isLast,
                    getStateStr(state),
                    arrayToString(rawMessage));
    }
}
