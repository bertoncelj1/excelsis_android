package com.excelsis.sms.recievedDatabase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Message.class}, version = 9)
public abstract class MessagesDatabase extends RoomDatabase {
    public abstract MessageDao messageDao();
}
