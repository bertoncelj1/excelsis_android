package com.excelsis;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.excelsis.sms.SmsReceiver;
import com.excelsis.sms.decoder.SmsMessageDecoder;
import com.excelsis.sms.recievedDatabase.DatabaseClient;
import com.excelsis.sms.recievedDatabase.Message;
import com.excelsis.sms.recievedDatabase.MessageDao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.excelsis.AppSettings.SettingsKey.REMEMBER_TEL_AND_SER_NUM;

public class DevSettingsActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "DevSettingsActivity";
    private List<Message> messages;
    private CheckBox cbRememberValues;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_settings);
        cbRememberValues = findViewById(R.id.cb_remember_values);
        cbRememberValues.setChecked((new AppSettings(this).isRememberingValuesSet()));

        reloadMessages(null);
        /*
        new Thread(() -> {
            Log.d("Message", "Printing all messages");
            MessageDao md = DatabaseClient.getDatabase(this).messageDao();
            List<Message> messages = md.getAll();
            StringBuilder messagesStr = new StringBuilder();
            for(Message msg : messages) {
                Log.d("Message", msg.toString());
                messagesStr.append(msg.toString());
            }
            TextView tv = findViewById(R.id.receivedMessages);
            tv.setText(messagesStr.toString());
        }).start();
         */
    }


    public void copyAssetsToExternalSD(View view) {
        RegisterLoaderFile.copyAssetsToExternalSD(this);
    }

    public void openExternalSDFolder(View view) {
        Uri selectedUri = Uri.parse(RegisterLoaderFile.getAppFolder().getAbsolutePath());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(selectedUri, "resource/folder");

        if (intent.resolveActivityInfo(getPackageManager(), 0) != null)
        {
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "No file explorer installed", Toast.LENGTH_SHORT).show();
        }
    }



    @SuppressLint("SetTextI18n")
    public void resendMessage(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resend message:");


        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);
        // write id of the last message revived
        if(messages.size() > 0) {
            input.setText(messages.get(0).id + "");
            input.selectAll();
        }

        builder.setPositiveButton("OK", (dialog, which) -> {
            int selectedMessage = Integer.parseInt(input.getText().toString());
            fakeSendMessage(selectedMessage);
            dialog.cancel();
        });


        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        final AlertDialog dialog = builder.create();
        dialog.show();

        input.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                int selectedMessage = Integer.parseInt(input.getText().toString());
                fakeSendMessage(selectedMessage);
                dialog.cancel();
            }
            return true; // true -> we consumed the click
        });
        // show keyboard
        input.requestFocus();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

    }

    private void fakeSendMessage(int messageId) {
        // find the selected message with id and display it
        for (Message message : messages) {
            if (message.id == messageId) {
                SmsReceiver.showMessage(
                        DevSettingsActivity.this,
                        "myself",
                        message.rawMessage);
                return;
            }
        }
    }

    public void filterMessages(View v) {
        MessageDao md = DatabaseClient.getDatabase(this).messageDao();
        new Thread(() -> {
            SmsReceiver.findAndShowCompleteMessages(this);
        }).start();
    }

    public void reloadMessages(View v) {
        TextView tv = findViewById(R.id.receivedMessages);
        tv.setText("Loading ...");

        Log.d("Message", "Printing all messages");
        MessageDao md = DatabaseClient.getDatabase(this).messageDao();

        LiveData<List<Message>> ld = md.getAllAsync();
        ld.observe(this, messages -> {
            DevSettingsActivity.this.messages = messages;
            for (Message msg : messages) {
                Log.d("Message", msg.toLongString());
            }
            showReceivedMessages(messages);
        });
    }

    public void addMessage(View v) {
        TextView tv = findViewById(R.id.receivedMessages);
        tv.setText("Loading ...");

        String sms = "[80, 7B, 23, 82, 45, 7B, 99, 1B, 00, 1C, 00, 01, 01, 16, 03, 01, 13, 03, 09, 00, 02, 09, 0D, E4, 00, 08, E0, 40, 18, 60, AA, 00, 08, EA, 3E, A8, F5, C3]";
        byte[] smsBytes = Utils.stringSmsToByteArray(sms);

        System.out.println(sms);
        System.out.println(Utils.arrayToString(smsBytes));

        Message msg = new Message();
        msg.senderTelNumber = "123123";
        msg.rawMessage = smsBytes;
        msg.fragmentNumber = 1;
        msg.messageNumber = 1;
        msg.isLast = true;

        // save messages to db
        MessageDao md = DatabaseClient.getDatabase(this).messageDao();
        new Thread(() -> {
            md.insertAll(Collections.singletonList(msg));
        }).start();
    }

    public void showReceivedMessages(List<Message> messages) {
        StringBuilder messagesStr = new StringBuilder();
        messagesStr.append("Messages: \n");
        if (messages.size() == 0) messagesStr.append("No messages.");
        for (Message msg : messages) {
            messagesStr.append(msg.toLongString()).append("\n\n");
        }
        TextView tv = findViewById(R.id.receivedMessages);
        tv.setText(messagesStr.toString());
    }

    public void openFTPServerSettings(View view) {
        startActivity(new Intent(this, DevFTPServerSettings.class));
    }

    public void onRememberValuesToggled(View view) {
        // update (aka. save) app settings everytime the check box is toggled
        (new AppSettings(this)).saveBool(REMEMBER_TEL_AND_SER_NUM, cbRememberValues.isChecked());
    }
}