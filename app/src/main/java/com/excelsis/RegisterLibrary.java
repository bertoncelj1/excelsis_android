package com.excelsis;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import au.com.bytecode.opencsv.CSVReader;

public class RegisterLibrary {

    private static final String DEBUG_TAG = "RegisterStorage";

    HashMap<String, Register> mRegisters;
    ArrayList<Register> mRegistersOrdered;

    // different parsers for different file types
    public enum RegisterParsers {
        SETTINGS, DECODING, READ
    }
    public final RegisterParser registerParser;


    private RegisterLibrary(RegisterParsers parser) {
        switch(parser) {
            case SETTINGS:
            default:
                this.registerParser = new RegisterParserFini(this);
                break;

            case DECODING:
                this.registerParser = new RegisterParserDecodingRegisters(this);
                break;

            case READ:
                this.registerParser = new RegisterParserReadFormat(this);
        }
    }


    private static RegisterLibrary settingsRegisterLibrary;
    public static RegisterLibrary getSettingsRegisters() {
        if (settingsRegisterLibrary == null) {
            settingsRegisterLibrary = new RegisterLibrary(RegisterParsers.SETTINGS);
        }
        return settingsRegisterLibrary;
    }


    private static RegisterLibrary decodeRegisters;
    public static RegisterLibrary getDecodeRegisters() {
        if (decodeRegisters == null) {
            decodeRegisters = new RegisterLibrary(RegisterParsers.DECODING);
        }
        return decodeRegisters;
    }

    private static RegisterLibrary readRegisters;
    public static RegisterLibrary getReadRegisters() {
        if (readRegisters == null) {
            readRegisters = new RegisterLibrary(RegisterParsers.READ);
        }
        return readRegisters;
    }


    public HashMap<String, Register> getRegisters() {
        return mRegisters;
    }

    public ArrayList<Register> getRegistersArray() {
        return new ArrayList<>(mRegisters.values());
    }

    public ArrayList<Register> getRegistersArrayOrdered() {
        return mRegistersOrdered;
    }



    /**
     * Loads registers from assets.
     * Only default or test registers are read from assets.
     * Other registers should't be read from assets.
     *
     * @param context  needs context to load app's assets
     * @param fileName filename to load from
     * @return true if success false if something went wrong
     */
    public void readFromAssets(Context context, final String fileName) {
        Reader reader = null;

        try {
            reader = new InputStreamReader(context.getAssets().open(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RegisterLoaderHelper.File.ReadingRegistersException(e.getMessage());
        }

        if (reader == null) {
            String msg = "Cannot load registers. Filename: " + fileName;
            Log.e(DEBUG_TAG, msg);
            throw new RegisterLoaderHelper.File.ReadingRegistersException(msg);
        }

        registerParser.parseRegisters(reader);
    }

    /**
     * Reads registers CSV mRegisters from file and adds them to existing registers
     *
     * @param fileName where to read registers from
     * @return true if success false if something went wrong
     */
    public boolean readFromFile(String fileName) {
        Reader reader = null;
        try {
            // Find the directory for the SD Card using the API
            File sdcard = Environment.getExternalStorageDirectory();
            reader = new FileReader(new File(sdcard, fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (reader == null) {
            Log.e(DEBUG_TAG, "Cannot load registers from file. Filename: " + fileName);
            return false;
        }

        return registerParser.parseRegisters(reader);
    }

    /**
     * Reads registers CSV mRegisters directly from string
     *
     * @param registers string to read from
     * @return true if success false if something went wrong
     */
    public boolean readFromString(String registers) {
        StringReader reader = new StringReader(registers);
        return registerParser.parseRegisters(reader);
    }

    public boolean read(Reader reader) {
        return registerParser.parseRegisters(reader);
    }

    public void addNewRegisterToStorage(Register register) {
        // check if register exist in database
        if (mRegisters.containsKey(register.preferenceKey)) {
            Log.w(DEBUG_TAG, "Register: '" + register + "' already exists. Current register: "
                    + mRegisters.get(register.preferenceKey));
        } else {
            Log.v(DEBUG_TAG, "Read new register: " + register + " " + register.defaultValue);
            mRegisters.put(register.preferenceKey, register);
            mRegistersOrdered.add(register);
        }
    }

    abstract static class RegisterParser {
        RegisterLibrary rl;

        public static final RegisterParsers type = null;

        public RegisterParser(RegisterLibrary rl) {
            this.rl = rl;
        }
        public abstract boolean parseRegisters(Reader registerReader);
        public abstract RegisterParsers getType();
    }

    public static class RegisterParserDecodingRegisters extends RegisterParser {
        @Override
        public RegisterParsers getType() {
            return RegisterParsers.DECODING;
        }

        public RegisterParserDecodingRegisters(RegisterLibrary rl) {
            super(rl);
        }

        @Override
        public boolean parseRegisters(Reader registerReader) {
            // empty registers before reading them
            rl.mRegisters = new HashMap<>();
            rl.mRegistersOrdered = new ArrayList<>();

            // CSV file specifications
            final char separator = ',';
            final char quote = '"';
            final boolean strictQuotes = false;

            // read all data from file and save it to readData
            CSVReader reader = new CSVReader(registerReader, separator, quote, strictQuotes);
            try {
                int rowNumber = 0;
                for (String[] row : reader.readAll()) {
                    rowNumber ++;
                    try {
                        int channel = Integer.parseInt(row[0], 16);
                        int registerNumber = Integer.parseInt(row[2], 16);
                        String preferenceKey = "" + Register.getKey(channel, registerNumber);

                        Register register = new Register(preferenceKey);
                        register.channel = channel;
                        register.register = registerNumber;
                        register.dataType = Integer.parseInt(row[3]);
                        register.defaultValue = row[4];
                        register.description = row[1] + ", " + row[5];
                        rl.addNewRegisterToStorage(register);
                    } catch (NumberFormatException e) {
                        Log.e(DEBUG_TAG, "Illegal number at row " + rowNumber + ": " + e.getMessage());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(DEBUG_TAG, "Failed reading CSV " + e.getMessage());
            }

            return true;
        }

    }

    public static class RegisterParserReadFormat extends RegisterParser {

        @Override
        public RegisterParsers getType() {
            return RegisterParsers.READ;
        }

        public RegisterParserReadFormat(RegisterLibrary rl) {
            super(rl);
        }

        public boolean parseRegisters(Reader registersReader) {
            if (registersReader == null) {
                Log.e(DEBUG_TAG, "Need a reader to read registers");
                return false;
            }

            // empty registers before reading them
            rl.mRegisters = new HashMap<>();
            rl.mRegistersOrdered = new ArrayList<>();

            List<String[]> readData = null;

            try {
                // CSV file specifications
                final char separator = ',';
                final char quote = '"';
                final boolean strictQuotes = false;

                // read all data from file and save it to readData
                CSVReader reader = new CSVReader(registersReader, separator, quote, strictQuotes);
                readData = reader.readAll();

                reader.close();
            } catch (IOException e) {
                Log.e(DEBUG_TAG, e.getMessage());
            }

            if (readData == null) {
                Log.i(DEBUG_TAG, "Unable to read any data");
                return false;
            }

            int rowNumber = 0;
            int nRegistersLoaded = 0;
            // adds readData to the mRegisters hash map
            for (String[] row : readData) {
                rowNumber++;

                if (row.length == 0 ||
                        row[0].startsWith("#") // just a comment line
                ) {
                    // if the line is empty or it starts with #, ignore it
                    Log.d(DEBUG_TAG, "Empty comment line: " + Arrays.toString(row));
                    continue;
                }

                // check if number of CSV data in a row is correct
                int EXPECTED_ROW_LENGTH = 3;
                if (row.length != EXPECTED_ROW_LENGTH) {
                    Log.w(DEBUG_TAG, String.format(Locale.getDefault(),
                            "Expecting CSV lines with %d parameters got %d. At row %d: %s",
                            row.length, EXPECTED_ROW_LENGTH, rowNumber, Arrays.toString(row)));
                    continue;
                }


                try {
                    // create new register and fill it with data from CSV row
                    String preferenceKey = row[0] + row[1]; // registers don't really have preference key, that's why we need to compese it from their channel and register number
                    Register register = new Register(preferenceKey);
                    register.channel = Integer.valueOf(row[0], 16); // register number channel / prefix
                    register.register = Integer.valueOf(row[1], 16); // register number register key
                    register.description = row[2];

                    rl.addNewRegisterToStorage(register);
                    nRegistersLoaded ++;
                } catch (NumberFormatException e) {
                    Log.e(DEBUG_TAG, "Illegal number at row " + rowNumber + ": " + e.getMessage());
                }
            }

            Log.d(DEBUG_TAG, String.format("Loaded %d registers. All registers: %d", nRegistersLoaded, rl.mRegisters.size()));

            return true;
        }

    }

    public static class RegisterParserFini extends RegisterParser {

        @Override
        public RegisterParsers getType() {
            return RegisterParsers.SETTINGS;
        }

        public RegisterParserFini(RegisterLibrary rl) {
            super(rl);
        }

        /**
         * Read register in a new "finc" format
         *
         * @param registersReader where to read registers from
         * @return true if success, false is something went wrong
         */
        public boolean parseRegisters(Reader registersReader) {
            if (registersReader == null) {
                Log.e(DEBUG_TAG, "Need reader to read registers");
                return false;
            }

            // empty registers before reading them
            rl.mRegisters = new HashMap<>();
            rl.mRegistersOrdered = new ArrayList<>();

            List<String[]> readData = null;

            try {
                // CSV file specifications
                final char separator = ',';
                final char quote = '"';
                final boolean strictQuotes = false;

                // read all data from file and save it to readData
                CSVReader reader = new CSVReader(registersReader, separator, quote, strictQuotes);
                readData = reader.readAll();

                reader.close();
            } catch (IOException e) {
                Log.e(DEBUG_TAG, e.getMessage());
            }

            if (readData == null) {
                Log.i(DEBUG_TAG, "Unable to read any data");
                return false;
            }

            int rowNumber = 0;
            int nRegistersLoaded = 0;
            // adds readData to the mRegisters hash map
            for (String[] row : readData) {
                rowNumber++;

                // the first two rows are headers
                if (rowNumber == 1 || rowNumber == 2) {
                    continue;
                }

                if (row.length == 0 || row.length == 1 ||
                        row[0].startsWith("#") || // just a comment line
                        (row.length > 2 && row[2].trim().equals(""))) { // preference key is empty
                    // if the line is empty or it starts with #, ignore it
                    Log.d(DEBUG_TAG, "Empty comment line: " + Arrays.toString(row));
                    continue;
                }

                // check if number of CSV data in a row is correct
                int EXPECTED_ROW_LENGTH = 10;
                if (row.length != EXPECTED_ROW_LENGTH) {
                    Log.w(DEBUG_TAG, String.format(Locale.getDefault(),
                            "Expecting CSV lines with %d parameters got %d. At row %d: %s",
                            row.length, EXPECTED_ROW_LENGTH, rowNumber, Arrays.toString(row)));
                    continue;
                }


                try {
                    // create new register and fill it with data from CSV row
                    String preferenceKey = row[2];
                    Register register = new Register(preferenceKey);

                    // register type
                    if (!row[3].equals("")) register.dataType = Integer.parseInt(row[3]);

                    // register number channel / prefix
                    if (!row[4].equals(""))
                        register.channel = Integer.valueOf(row[4], 16);

                    // register number register key
                    if (!row[5].equals("")) register.register = Integer.valueOf(row[5], 16); // address

                    register.defaultValue = row[6];
                    register.description = row[7];

                    rl.addNewRegisterToStorage(register);
                    nRegistersLoaded ++;
                } catch (NumberFormatException e) {
                    Log.e(DEBUG_TAG, "Illegal number at row " + rowNumber + ": " + e.getMessage());
                }
            }

            Log.d(DEBUG_TAG, String.format("Loaded %d registers. All registers: %d", nRegistersLoaded, rl.mRegisters.size()));

            return true;
        }

    }

}
